<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
  <table style="color: #747678; width: 100%;">
    <tr>
      <td colspan="12">
        <img style="width: 100%;" src="{{asset('images/topo-fv.png')}}" />
      </td>
    </tr>
    <tr>
      <td colspan="12" style="font-size: 18px; padding-top: 30px; text-align: center;">
        DECLARAÇÃO
      </td>
    </tr>
    <tr>
      <td colspan="12" style="font-size: 16px; padding-top: 40px; line-height: 25px;">
        Declaramos para os devidos fins que o participantes
        @foreach ($users as $user)
          <b>{{ $user->name }}</b>,&nbsp;
        @endforeach
        completaram a execução da atividade prática intitulada {{ ($workshop) ? $workshop->name : 'NÃO ENCONTRADO' }},
        com carga horária de 20h horas, realizada no Ambiente Virtual de Aprendizagem do Programa Ciclo Saúde. Essa formação é parte integrante do
        Programa Ciclo Saúde: Cooperação Técnica para Fortalecimento da Atenção Básica.
      </td>
    </tr>
    <tr>
      <td colspan="12" style="font-size: 16px; padding-top: 40px; line-height: 25px; text-align: center;">
        Rio de Janeiro, {{ date('d/m/Y') }}.
      </td>
    </tr>

    <tr>
      <td colspan="12" style="font-size: 16px; padding-top: 40px;">
        <img style="width: 100%;" src="{{ asset('images/assinaturas-vale.png') }}" />
      </td>
    </tr>

    <tr>
      <td colspan="12" style="font-size: 16px; padding-top: 30px; text-align: center;">
        <img style="width: 450px" src="{{ asset('images/barra-logo-fv.png') }}" />
      </td>
    </tr>
  </table>

</body>
</html>
