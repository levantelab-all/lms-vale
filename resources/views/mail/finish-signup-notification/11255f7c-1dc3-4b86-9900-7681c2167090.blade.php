<table style="width: 100%; color: #747678; font-size: 18px; line-height: 25px;">
  <tr style="text-align: center;">
    <td colspan="12">
      <img style="width: 220px;" src="{{ asset('images/logo-email.png') }}" />
    </td>
  </tr>
  <tr>
    <td style="padding-top: 80px;" colspan="12">
      <p>Olá, {{$user->name}}</p>
      <p>Que bom ter você com a gente no Ambiente Virtual de Aprendizagem Ciclo Saúde!</p>
      <p>Para acessar sua conta você precisar acessar o ambiente em <a href="{{ $tenant->frontend_url }}">{{ $tenant->frontend_url }}</a>  e efetuar o login utilizando seu email cadastrado.</p>
      <br>
      <p>Sua senha inicial é <b>{{ $password }}</b>.</p>
      <br>
      <p>Bem vindo!</p>
    </td>
  </tr>
  <tr>
    <td style="padding-top: 48px;" colspan="12">
      <span>Atenciosamente,</span><br>
      <span style="font-weight: 700">Equipe Ciclo Saúde</span>
    </td>
  </tr>
  <tr style="text-align: center;">
    <td style="padding-top: 48px; padding-bottom: 48px;" colspan="12">
      <img style="width: 450px;" src="{{ asset('images/barra-logo-fv.png') }}" />
    </td>
  </tr>

  <tr>
    <td style="border-top: 1px solid #CCCCCC;" colspan="12">
      <span>
        Esta é uma mensagem automática, favor não responder este e-mail.
      </span>
    </td>
  </tr>
</table>
