# Instalação

1. `cp .env.example .env`
2. `touch database/database.sqlite`
3. `composer install --no-dev`
4. `php artisan key:generate`
5. `php artisan migrate --seed`
6. `php artisan db:seed ContentSeeder`
7. `php artisan db:seed RolesSeeder`
8. `php artisan storage:link`
9. configurar as demais variáveis do arquivo .env (ex: SPARKPOST_SECRET)

# Atualização

1. incorporar possíveis mudanças do .env.example
2. `composer install --no-dev`
3. `php artisan migrate` (caso existam novas migrações)

# Dia-a-dia

1. `php artisan serve` para subir o servidor local
2. `php artisan migrate:fresh --seed` para resetar a base de dados local
3. `php artisan db:seed ContentSeeder` para carregar os dados globais (executar sempre que a base for resetada)
4. `php artisan db:seed RolesSeeder` para carregar os dados de cargos se necessário
