<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id')
                ->constrained()
                ->onDelete('cascade');
            $table->foreignId('tenant_id')
                ->constrained();
            $table->uuid('uuid');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('cover_url')->nullable();
            $table->boolean('visible')->default(false);
            $table->longText('properties')->nullable();
            $table->timestamps();

            $table->unique([
                'uuid',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
