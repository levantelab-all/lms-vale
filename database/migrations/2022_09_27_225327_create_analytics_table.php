<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tenant_id')
                ->constrained();
            $table->foreignId('user_id')
                ->constrained();

            $table->string('type')->index();
            $table->string('label1')->index();
            $table->string('label2')->nullable()->index();
            $table->string('label3')->nullable()->index();
            $table->integer('progress')->nullable();

            $table->string('city')->nullable()->index();
            $table->string('ubs')->nullable()->index();

            $table->timestamps();

            $table->index([
                'tenant_id',
                'user_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics');
    }
}
