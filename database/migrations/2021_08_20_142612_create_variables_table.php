<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tenant_id')
                ->constrained();
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->string('group');
            $table->string('key');
            $table->longText('value')->nullable();
            $table->timestamps();

            $table->unique([
                'tenant_id',
                'owner_id',
                'group',
                'key',
            ]);

            $table->foreign('owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
