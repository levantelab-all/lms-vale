<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('tenant_id')->nullable();

            $table->string('type');
            $table->uuid('uuid');

            $table->unsignedBigInteger('ref_id')->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('owner_id')->nullable();

            $table->string('name');
            $table->longText('properties')->nullable();
            $table->boolean('private')->default(true);
            $table->boolean('visible')->default(false);
            $table->integer('order')->default(0);

            $table->timestamps();

            $table->unique([
                'tenant_id',
                'type',
                'uuid',
            ]);

            $table->foreign('parent_id')
                ->references('id')
                ->on('contents')
                ->onDelete('set null');

            $table->foreign('owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
