<?php

use App\Models\Content;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tenant_id')
                ->constrained();
            $table->foreignId('user_id')
                ->constrained()
                ->onDelete('cascade');
            $table->unsignedBigInteger('ref_id');
            $table->uuid('uuid');
            $table->string('template');
            $table->longText('parameters')->nullable();
            $table->string('url');
            $table->timestamps();

            $table->unique([
                'uuid',
            ]);

            $table->unique([
                'tenant_id',
                'user_id',
                'ref_id',
            ]);

            $table->foreign('ref_id')
                ->references('id')
                ->on('contents')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
