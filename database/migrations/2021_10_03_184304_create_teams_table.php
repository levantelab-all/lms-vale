<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tenant_id')
                ->constrained();
            $table->uuid('uuid');
            $table->string('name');
            $table->unsignedBigInteger('ref_id')->nullable();
            $table->longText('properties')->nullable();
            $table->timestamps();

            $table->unique([
                'uuid',
            ]);

            $table->foreign('ref_id')
                ->references('id')
                ->on('contents')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
