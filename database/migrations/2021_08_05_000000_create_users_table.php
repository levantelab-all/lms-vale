<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tenant_id')
                ->constrained();
            $table->uuid('uuid');
            $table->string('name');
            $table->string('email');
            $table->string('cpf', 11);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('level')->default(App\Models\User::LEVEL_USER);
            $table->unsignedBigInteger('group_id')->nullable();
            $table->string('group_name')->nullable();
            $table->longText('properties')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->unique([
                'uuid',
            ]);

            $table->unique([
                'tenant_id',
                'email',
            ]);

            $table->unique([
                'tenant_id',
                'cpf',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
