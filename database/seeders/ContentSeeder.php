<?php

namespace Database\Seeders;

use App\Imports\CitiesImport;
use App\Imports\StatesImport;
use App\Imports\UBSImport;
use App\Models\Content;
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    public function run()
    {
        config(['app.debug_sql' => false]);

        Content::truncate();

        \Excel::import(new StatesImport, database_path('fixtures/states.xlsx'));
        \Excel::import(new CitiesImport, database_path('fixtures/cities.xlsx'));
    }
}
