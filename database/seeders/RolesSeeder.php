<?php

namespace Database\Seeders;

use App\Imports\RolesImport;
use App\Models\Content;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
        config(['app.debug_sql' => false]);

        Content::where('type', 'roles')
            ->delete();

        \Excel::import(new RolesImport, database_path('fixtures/roles.xlsx'));
    }
}
