<?php

namespace Database\Seeders;

use App\Models\Tenant;
use App\Models\User;
use Illuminate\Database\Seeder;

class TenantAndUserSeeder extends Seeder
{
    public function run()
    {
        config(['app.debug_sql' => false]);

        \DB::table('users')
            ->delete();
        \DB::table('tenants')
            ->delete();

        foreach (config('tenants.tenants', []) as $uuid => $tenant) {
            $tenant['uuid'] = $uuid;

            $tenant = Tenant::create($tenant);

            if (app()->environment() !== 'production') {
                User::create([
                    'tenant_id' => $tenant->id,
                    'uuid' => User::generateUUID(),
                    'name' => 'Administrador Levante Lab',
                    'email' => 'admin@levantelab.com.br',
                    'cpf' => '21109100035',
                    'password' => bcrypt('1a2a3a'),
                    'level' => User::LEVEL_ADMIN,
                ]);
            }
        }
    }
}
