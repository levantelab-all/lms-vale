# Local

## 0. common

Administradores podem criar, alterar e apagar qualquer recurso. Usuários podem manipular apenas conteúdos dos quais são donos.

**properties:**

- id - UUID
- ref_id - ID de referência externa (padrão NULL)
- parent_id - ID do conteúdo PAI (padrão NULL)
- owner_id - ID do proprietário do conteúdo
- name - nome do conteúdo
- properties - propriedades JSON (padrão NULL)
- private - 1 - privado; 0 - público
- visible - 1 - visível; 0 - invisível
- order - campo para ordenação (padrão 0)

**fields:**

- ref_id - nullable|integer
- parent_id - nullable|exists:contents,id
- name - required|string|max:191
- private - nullable|boolean
- visible - nullable|boolean
- order - nullable|integer

**filters:**

- name - partial
- id - exact
- ref_id - exact
- private - exact
- visible - exact

**sorts:**

- name
- order (padrão)
- created_at
- updated_at

**read-permissions**:

- guest
- user
- admin

**write-permissions:**

- admin

**can-be-orphan:** true



## 1. ubs

**properties:**

- state_id - UUID do estado (referência indireta ao global)
- state - nome do estado
- city_id - UUID da cidade (referência indireta ao global)
- city - nome da cidade

**fields:**

- state_id - required|uuid
- state - required|string
- city_id - required|uuid
- city - required|string

**includes:**

- teams - collection
- teams.users - collection

**filters:**

- state_id - exact
- state - partial
- city_id - exact
- city - partial
- teams.id - exact
- teams - scope (*true* para incluir apenas UBS com equipes e *false* para incluir apenas UBS sem equipes)
- search - scope

**orders:**

- state
- city

**page-size:** 25

**can-be-orphan:** true



## 2. workshops

**properties:**

- type - tipo da oficina
- description - descrição da oficina
- cover_url - URL da imagem de capa
- dedication_time - carga horária do workshop

**fields:**

- type - required|string|in:basic,thematic
- description - nullable|string
- cover_url - nullable|url
- dedication_time - required|integer|min:0

**relations:**

- modules - hasMany
- evaluations - hasMany

**includes:**

- modules - collection
- evaluations - collection

**filters:**

- type - exact

**page-size:** 12



## 3. modules

**parent_id:** UUID da workshop

**properties:**

- type - tipo do módulo

**fields:**

- type - required|string|in:content,diary

**relations:**

- activities - hasMany
- workshop - belongsTo

**includes:**

- activities - collection
- workshop - model

**filters:**

- type - exact

**page-size:** 0

**can-be-orphan:** false



## 4. activities

**parent_id:** UUID do module

**properties:**

- type - tipo da atividade
- body - texto de acompanhamento do conteúdo
- url - URL do conteúdo (imagem, vídeo ou documento)
- embed - script de embed (embed)

**fields:**

- type - required|string|in:image,text,embed,video,document
- body - nullable
- url - required_if:type,image,video,document|nullable|url
- embed - required_if:type,embed|nullable

**relations:**

- module - belongsTo

**includes:**

- module - model

**filters:**

- type - exact

**page-size:** 0

**can-be-orphan:** false



## 5. evaluations

**parent_id:** UUID da workshop

**properties:**

- description - descrição / instruções

**fields:**

- description - nullable

**relations:**

- questions - hasMany
- workshop - belongsTo

**includes:**

- questions - collection
- workshop - model

**filters:**

- ...

**page-size:** 0

**can-be-orphan:** false



## 6. questions

**parent_id:** UUID da evaluation

**properties:**

- type - tipo da pergunta
- question - texto da pergunta
- feedback - feedback da resposta
- options - array de opções com id e texto (múltipla escolha)

**fields:**

- type - required|string|in:boolean,multi-choice
- question - required|string
- feedback - nullable|string
- options.* - required_if:type,multi-choice|nullable|array
- options.*.id - required|integer
- options.*.text - required|string

**relations:**

- evaluation - belongsTo

**includes:**

- evaluation - model

**filters:**

- ...

**page-size:** 0

**can-be-orphan:** false



## 7. roles

**sorts:**

- name (padrão)

**page-size:** 25

**can-be-orphan:** true



## 8. documents

**properties:**

- type - tipo do documento
- url - URL do conteúdo
- thumb - URL da imagem de capa
- summary - resumo do conteúdo

**fields:**

- type - required|string|in:document,image,link,video,embed
- url - required_if:type,document,image,link,video|string|url
- embed - required_if:type,embed|string
- thumb - nullable|string|url
- summary - nullable|string

**filters:**

- type - exact
- search - scope

**page-size:** 25
