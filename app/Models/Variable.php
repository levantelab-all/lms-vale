<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

class Variable extends Model
{
    use HasFactory, RevisionableTrait;

    protected $fillable = [
        'tenant_id',
        'owner_id',
        'group',
        'key',
        'value',
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}
