<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\RevisionableTrait;

class Project extends Model
{
    use HasFactory, RevisionableTrait;

    protected $fillable = [
        'team_id',
        'tenant_id',
        'uuid',
        'title',
        'description',
        'cover_url',
        'visible',
        'properties',
        'certificate_url',
        'certificate_created_at',
    ];

    protected $casts = [
        'visible' => 'boolean',
        'properties' => AsArrayObject::class,
        'certificate_created_at' => 'datetime',
    ];

    protected $withCount = [
        'posts',
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [];

    public const PROTECTED_FIELDS = [
        'id',
        'team_id',
        'tenant_id',
        'uuid',
        'title',
        'description',
        'cover_url',
        'visible',
        'properties',
        'certificate_url',
        'certificate_created_at',
        'created_at',
        'updated_at',
        'team',
        'posts',
    ];

    public const DEFAULT_RELATIONS = [
    ];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class)
            ->orderBy('posts.created_at', 'DESC');
    }

    public static function findByUUID($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    public static function generateUUID()
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function scopeStates($query, ...$terms)
    {
        return $query->whereHas('team', function ($q) use ($terms) {
            return $q->whereHas('content', function ($q2) use ($terms) {
                return $q2->whereIn('contents.properties->state', $terms);
            });
        });
    }
}
