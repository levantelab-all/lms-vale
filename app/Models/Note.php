<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\RevisionableTrait;

class Note extends Model
{
    use HasFactory, RevisionableTrait;

    protected $fillable = [
        'user_id',
        'tenant_id',
        'uuid',
        'title',
        'body',
        'properties',
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [ ];

    protected $casts = [
        'properties' => AsArrayObject::class,
    ];

    public const PROTECTED_FIELDS = [
        'id',
        'user_id',
        'tenant_id',
        'uuid',
        'title',
        'body',
        'properties',
        'created_at',
        'updated_at',
    ];

    public const DEFAULT_RELATIONS = [
    ];

    public static function findByUUID($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    public static function generateUUID()
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeSearch($query, $term)
    {
        $term = '%' . str_replace(' ', '%', $term) . '%';

        return $query->where(function ($q) use ($term) {
            return $q->where('title', 'LIKE', $term)
                ->orWhere('body', 'LIKE', $term);
        });
    }
}
