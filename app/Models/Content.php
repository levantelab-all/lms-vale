<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\RevisionableTrait;

class Content extends Model
{
    use RevisionableTrait;

    protected $fillable = [
        'tenant_id',
        'type',
        'uuid',
        'ref_id',
        'parent_id',
        'owner_id',
        'name',
        'properties',
        'private',
        'visible',
        'order',
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [ ];

    protected $casts = [
        'private' => 'boolean',
        'visible' => 'boolean',
        'order' => 'integer',
        'properties' => AsArrayObject::class,
    ];

    public const _DIRECT_FIELDS = [
        'ref_id',
        'parent_id',
        'name',
        'private',
        'visible',
        'order',
        'properties',
    ];

    public const _PROTECTED_RELATIONS = [
        'owner',
        'teams',
    ];

    public static function findByUUID($type, $uuid)
    {
        return static::where('type', $type)
            ->where('uuid', $uuid)
            ->first();
    }

    public static function findByRef($type, $refId)
    {
        return static::where('type', $type)
            ->where('ref_id', $refId)
            ->first();
    }

    public static function generateUUID($type)
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($type, $uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function teams()
    {
        return $this->hasMany(Team::class, 'ref_id')
            ->orderBy('teams.name');
    }

    public function scopeTeams($query, $value)
    {
        if ($value) {
            $query = $query->has('teams');
        } else {
            $query = $query->doesntHave('teams');
        }
    }

    public static function bootRelations($baseConfig, $scope)
    {
        foreach ($baseConfig as $name => $relationship) {
            if (in_array($name, self::_PROTECTED_RELATIONS)) {
                continue;
            }

            static::resolveRelationUsing($name, function ($model) use ($name, $relationship, $scope) {
                $relationType = $relationship['type'];

                if ($relationType === 'belongsTo') {
                    $model = $model->belongsTo(Content::class, 'parent_id', 'id');
                } else if ($relationType === 'hasMany') {
                    $model = $model->hasMany(Content::class, 'parent_id', 'id');
                }

                if (isset($relationship['filters']) && is_array($relationship['filters'])) {
                    foreach ($relationship['filters'] as $field => $value) {
                        $model = $model->where($field, $value);
                    }
                }

                if ($scope === 'global') {
                    $model = $model->whereNull('tenant_id');
                } else {
                    $model = $model->where('tenant_id', tenant('id'));
                }

                if (isset($relationship['orders']) && is_array($relationship['orders'])) {
                    foreach ($relationship['orders'] as $field => $direction) {
                        $model = $model->orderBy($field, $direction);
                    }
                }

                return $model;
            });

            content_setup_relationships($scope, $name);
        }
    }
}
