<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Venturecraft\Revisionable\RevisionableTrait;

class Team extends Model
{
    use HasFactory, RevisionableTrait, HasJsonRelationships;

    protected $fillable = [
        'tenant_id',
        'uuid',
        'name',
        'ref_id',
        'properties',
    ];

    protected $casts = [
        'properties' => AsArrayObject::class,
    ];

    protected $with = [
        'content',
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [];

    public const PROTECTED_FIELDS = [
        'id',
        'tenant_id',
        'uuid',
        'name',
        'ref_id',
        'properties',
        'created_at',
        'updated_at',
        'users',
    ];

    public const DEFAULT_RELATIONS = [
        'users',
        'projects',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }

    public function content()
    {
        return $this->belongsTo(Content::class, 'ref_id');
    }

    public function workshop()
    {
        return $this->belongsTo(Content::class, 'properties->mandatory_workshop->id', 'uuid');
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public static function findByUUID($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    public static function generateUUID()
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function scopeRef($query, ...$values)
    {
        $ids = Content::whereIn('uuid', $values)
            ->where('tenant_id', tenant('id'))
            ->pluck('id');

        return $query->whereIn('ref_id', $ids);
    }

    public function scopeSearch($query, $term)
    {
        $term = '%' . str_replace(' ', '%', $term) . '%';

        return $query->where(function ($q) use ($term) {
            return $q->where('teams.name', 'LIKE', $term)
                ->orWhereHas('workshop', function ($q2) use ($term) {
                    return $q2->where('contents.name', 'LIKE', $term);
                })
                ->orWhereHas('content', function ($q2) use ($term) {
                    return $q2->where('contents.name', 'LIKE', $term)
                        ->orWhere('contents.properties->city', 'LIKE', $term);
                });
        });
    }

    public function scopeStates($query, ...$terms)
    {
        return $query->whereHas('content', function ($q2) use ($terms) {
            return $q2->whereIn('contents.properties->state', $terms);
        });
    }
}
