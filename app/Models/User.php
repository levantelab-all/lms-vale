<?php

namespace App\Models;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Venturecraft\Revisionable\RevisionableTrait;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, RevisionableTrait;

    public const LEVEL_DELETED = 'deleted';
    public const LEVEL_ADMIN = 'admin';
    public const LEVEL_USER = 'user';

    protected $fillable = [
        'tenant_id',
        'uuid',
        'name',
        'email',
        'phone',
        'cpf',
        'password',
        'level',
        'properties',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'properties' => AsArrayObject::class,
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [
        'remember_token',
    ];

    public const PROTECTED_FIELDS = [
        'id',
        'tenant_id',
        'uuid',
        'name',
        'email',
        'phone',
        'cpf',
        'password',
        'password_confirmation',
        'level',
        'properties',
        'email_verified_at',
        'remember_token',
        'created_at',
        'updated_at',
        'teams',
        'certificates',
    ];

    public const DEFAULT_RELATIONS = [
        'teams',
        'teams.workshop',
        'teams.projects',
        'certificates',
    ];

    public function teams()
    {
        return $this->belongsToMany(Team::class)
            ->withTimestamps();
    }

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }

    public static function findByUUID($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    public static function generateUUID()
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function scopeActive($query, $value)
    {
        if ($value) {
            $query = $query->whereNotIn('level', [
                self::LEVEL_DELETED
            ]);
        } else {
            $query = $query->whereIn('level', [
                self::LEVEL_DELETED
            ]);
        }

        return $query;
    }

    public function scopeSearch($query, $term)
    {
        $term = '%' . str_replace(' ', '%', $term) . '%';

        return $query->where(function ($q) use ($term) {
            return $q->where('users.name', 'LIKE', $term)
                ->orWhere('users.cpf', 'LIKE', $term)
                ->orWhereHas('teams', function ($q2) use ($term) {
                    return $q2->where('teams.name', 'LIKE', $term)
                        ->orWhereHas('content', function ($q3) use ($term) {
                            return $q3->where('contents.name', 'LIKE', $term)
                                ->orWhere('contents.properties->city', 'LIKE', $term);
                        })->orWhereHas('workshop', function ($q3) use ($term) {
                            return $q3->where('contents.name', 'LIKE', $term);
                        });
                });
        });
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function sendPasswordResetNotification($token)
    {
        ResetPasswordNotification::toMailUsing(function ($user, $token) {
            $tenant = tenant();

            $url = $tenant->frontend_url
                . '/redefinir-senha/'
                . urlencode(trim($user->getEmailForPasswordReset()))
                . '/'
                . urlencode(trim($token));

            $view = "mail.forgot-password-notification.{$tenant->uuid}";

            return (new MailMessage)
                ->subject('Alteração de senha')
                ->view($view, [
                    'tenant' => $tenant,
                    'user' => $user,
                    'url' => $url,
                ]);
        });

        $this->notify(new ResetPasswordNotification($token));
    }
}
