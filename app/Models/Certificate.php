<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\RevisionableTrait;

class Certificate extends Model
{
    use HasFactory, RevisionableTrait;

    protected $fillable = [
        'tenant_id',
        'user_id',
        'ref_id',
        'uuid',
        'template',
        'parameters',
        'url',
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [ ];

    protected $casts = [
        'parameters' => AsArrayObject::class,
    ];

    public const PROTECTED_FIELDS = [
        'id',
        'tenant_id',
        'user_id',
        'ref_id',
        'uuid',
        'template',
        'parameters',
        'url',
        'created_at',
        'updated_at',
    ];

    public const DEFAULT_RELATIONS = [
    ];

    public static function findByUUID($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    public static function generateUUID()
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ref()
    {
        return $this->belongsTo(Content::class);
    }

    public function render()
    {
        $content = view("certificates.{$this->template}", [
            'uuid' => $this->uuid,
            'user' => $this->user,
            'parameters' => $this->parameters,
        ])->render();

        $pdf = pdf();
        $pdf->writeHTML($content);
        $pdfContent = $pdf->Output('document.pdf', 'S');

        $path = tenant('uuid')
            . '/certificates/'
            . date('Ym')
            . date('Ymd-His-')
            . uniqid()
            . '.pdf';

        \Storage::disk('s3')->put($path, $pdfContent);

        return \Storage::disk('s3')->url($path);
    }
}
