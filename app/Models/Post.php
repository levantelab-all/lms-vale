<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\RevisionableTrait;

class Post extends Model
{
    use HasFactory, RevisionableTrait;

    protected $fillable = [
        'project_id',
        'user_id',
        'tenant_id',
        'uuid',
        'title',
        'body',
        'type',
        'photos',
    ];

    protected $casts = [
        'photos' => AsArrayObject::class,
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [];

    public const PROTECTED_FIELDS = [
        'id',
        'project_id',
        'user_id',
        'tenant_id',
        'uuid',
        'title',
        'body',
        'type',
        'photos',
        'created_at',
        'updated_at',
        'team',
    ];

    public const DEFAULT_RELATIONS = [
        'user',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function findByUUID($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    public static function generateUUID()
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function scopeSearch($query, $term)
    {
        $term = '%' . str_replace(' ', '%', $term);

        return $query->where(function ($q) use ($term) {
            return $q->where('title', 'LIKE', $term)
                ->orWhere('body', 'LIKE', $term);
        });
    }
}
