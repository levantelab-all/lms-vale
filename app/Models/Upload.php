<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Upload extends Model
{
    use HasFactory;

    protected $fillable = [
        'tenant_id',
        'uuid',
        'owner_id',
        'name',
        'path',
        'url',
    ];

    public static function findByUUID($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    public static function generateUUID()
    {
        $uuid = Str::uuid();

        while (!is_null(static::findByUUID($uuid))) {
            $uuid = Str::uuid();
        }

        return $uuid;
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}
