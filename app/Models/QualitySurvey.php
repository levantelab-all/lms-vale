<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QualitySurvey extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'ref_id',
        'question1',
        'question2',
        'question3',
        'question4',
        'question5',
        'question6',
        'question7',
        'question8',
        'question9',
        'question10',
        'question11',
        'question12',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ref()
    {
        return $this->belongsTo(Content::class);
    }
}
