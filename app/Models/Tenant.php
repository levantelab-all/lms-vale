<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

class Tenant extends Model
{
    use RevisionableTrait;

    protected $fillable = [
        'id',
        'uuid',
        'name',
        'frontend_url',
    ];

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $dontKeepRevisionOf = [];
}
