<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Analytic extends Model
{
    use HasFactory;

    protected $fillable = [
        'tenant_id',
        'user_id',
        'type',
        'label1',
        'label2',
        'label3',
        'progress',
        'city',
        'ubs',
    ];

    protected $casts = [
        'progress' => 'integer',
    ];
}
