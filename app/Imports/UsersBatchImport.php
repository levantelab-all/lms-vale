<?php

namespace App\Imports;

use App\Notifications\FinishSignupNotification;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersBatchImport implements WithHeadingRow, WithMapping
{
    public function map($row): array
    {
        $data = [];

        $data['name'] = trim(Arr::get($row, 'nome'));

        if (Arr::has($row, 'cpf')) {
            $data['cpf'] = Arr::get($row, 'cpf');
        } else if (Arr::has($row, 'cpf_apenas_numeros')) {
            $data['cpf'] = Arr::get($row, 'cpf_apenas_numeros');
        }
        $data['cpf'] = clean_cpf($data['cpf']);

        if (Arr::has($row, 'celular')) {
            $data['phone'] = Arr::get($row, 'celular');
        } else if (Arr::has($row, 'telefone')) {
            $data['phone'] = Arr::get($row, 'telefone');
        } else if (Arr::has($row, 'whatsapp_dddnumero')) {
            $data['phone'] = Arr::get($row, 'whatsapp_dddnumero');
        } else if (Arr::has($row, 'whatsapp')) {
            $data['phone'] = Arr::get($row, 'whatsapp');
        }
        $data['phone'] = clean_phone($data['phone']);
        if (empty($data['phone'])) {
            $data['phone'] = null;
        }

        if (Arr::has($row, 'email')) {
            $data['email'] = Arr::get($row, 'email');
        } else if (Arr::has($row, 'e_mail')) {
            $data['email'] = Arr::get($row, 'e_mail');
        }
        $data['email'] = trim(Str::lower($data['email']));

        return $data;
    }
}
