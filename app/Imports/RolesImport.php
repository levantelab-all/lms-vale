<?php

namespace App\Imports;

use App\Models\Content;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class RolesImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $roles)
    {
        $tenants = config('tenants.tenants', []);

        try {
            \DB::beginTransaction();

            foreach($roles as $index => $model) {
                foreach ($tenants as $tenant) {
                    Content::create([
                        'tenant_id' => $tenant['id'],
                        'type' => 'roles',
                        'uuid' => Content::generateUUID('roles'),
                        'ref_id' => null,
                        'parent_id' => null,
                        'owner_id' => null,
                        'name' => $model['role'],
                        'properties' => null,
                        'private' => false,
                        'visible' => true,
                        'order' => $index + 1,
                    ]);
                }
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
