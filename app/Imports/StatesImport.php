<?php

namespace App\Imports;

use App\Models\Content;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StatesImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Content([
            'tenant_id' => null,
            'type' => 'states',
            'uuid' => Content::generateUUID('states'),
            'ref_id' => $row['id'],
            'parent_id' => null,
            'owner_id' => null,
            'name' => $row['name'],
            'properties' => null,
            'private' => false,
            'visible' => true,
            'order' => 0,
        ]);
    }
}
