<?php

namespace App\Imports;

use App\Models\Content;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CitiesImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        $parentId = optional(Content::findByRef('states', $row['uf']))->id;

        $shortId = (int) substr("{$row['id']}", 0, 6);

        return new Content([
            'tenant_id' => null,
            'type' => 'cities',
            'uuid' => Content::generateUUID('cities'),
            'ref_id' => $shortId,
            'parent_id' => $parentId,
            'owner_id' => null,
            'name' => $row['name'],
            'properties' => [
                'id7' => (string) $row['id'],
                'state_id' => (string) $row['uf'],
            ],
            'private' => false,
            'visible' => true,
            'order' => 0,
        ]);
    }
}
