<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PurgeOrphans extends Command
{
    protected $signature = 'lms:purge-orphans';
    protected $description = 'Purge orphan contents';

    public function handle()
    {
        $this->handleScope('global');
        $this->handleScope('local');

        return 0;
    }

    private function handleScope($scope)
    {
        $types = config("{$scope}.types");

        foreach ($types as $type) {
            $this->handleType($scope, $type);
        }
    }

    private function handleType($scope, $type)
    {
        $canBeOrphan = config("{$scope}.{$type}.can-be-orphan", true);

        if ($canBeOrphan) {
            $this->warn("[{$scope}.{$type}] ignored");
        } else {
            try {
                \DB::beginTransaction();

                $query = \DB::table('contents')
                    ->where('type', $type)
                    ->whereNull('parent_id');

                if ($scope === 'global') {
                    $query = $query->whereNull('tenant_id');
                } else {
                    $query = $query->whereNotNull('tenant_id');
                }

                $query->delete();

                \DB::commit();

                $this->info("[{$scope}.{$type}] purged");
            } catch (\Exception $e) {
                \DB::rollback();

                $this->error("[{$scope}.{$type}] " . $e->getMessage());
            }

        }
    }
}
