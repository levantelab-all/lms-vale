<?php

namespace App\Http\Controllers;

use App\Http\Requests\MailRequest;
use App\Mail\SendMail;
use Illuminate\Http\Request;

class MailController extends Controller
{
    public function __invoke(MailRequest $request)
    {
        $mail = \Mail::to($request->input('to'));

        if (!empty($request->input('cc'))) {
            $mail = $mail->cc($request->input('cc'));
        }
        if (!empty($request->input('bcc'))) {
            $mail = $mail->bcc($request->input('bcc'));
        }

        $mail->queue(new SendMail(
            tenant(),
            $request->input('subject'),
            $request->input('body'),
            $request->input('text')
        ));

        return response()->noContent();
    }
}
