<?php

namespace App\Http\Controllers\Project\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(PaginationRequest $request, $projectUuid)
    {
        $pageSize = (int) request('size', 20);

        $project = find_project($projectUuid);
        abort_if(is_null($project), 404, 'Not found');

        $query = Post::with(['user'])
            ->where('tenant_id', tenant('id'))
            ->where('project_id', $project->id);

        $queryBuilder = QueryBuilder::for($query)
            ->allowedFilters([
                AllowedFilter::exact('id', 'uuid'),
                AllowedFilter::partial('title'),
                AllowedFilter::partial('body'),
                AllowedFilter::exact('type'),
                AllowedFilter::scope('search'),
            ])->allowedSorts([
                AllowedSort::field('title'),
                AllowedSort::field('created_at'),
                AllowedSort::field('updated_at'),
            ])->defaultSort('-created_at');

        if ($pageSize > 0) {
            $posts = $queryBuilder->paginate($pageSize);

            return PostResource::collection($posts);
        } else {
            $posts = $queryBuilder->get();

            return PostResource::collection($posts)
                ->additional([
                    'meta' => [
                        'total' => $posts->count(),
                    ],
                ]);
        }
    }
}
