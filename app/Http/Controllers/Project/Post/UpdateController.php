<?php

namespace App\Http\Controllers\Project\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\Post\UpdateRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, $projectUuid, $postUuid)
    {
        $project = find_project($projectUuid);
        abort_if(is_null($project), 404, 'Not found');
        abort_if($project->visible, 409, 'Project already published');

        $query = Post::where('uuid', $postUuid)
            ->where('tenant_id', tenant('id'))
            ->where('project_id', $project->id);
        if (!is_admin()) {
            $query = $query->where('user_id', auth()->user()->id);
        }
        $post = $query->first();
        abort_if(is_null($post), 404, 'Not found');

        $images = $request->file('images');
        $newPhotos = [];
        if (!empty($images)) {
            foreach ($images as $file) {
                $extension = $file->guessExtension();

                $basePath = tenant('uuid') . '/' . date('Ym');
                $filename = date('Ymd-His-') . uniqid() . '.' . $extension;

                $path = $file->storeAs($basePath, $filename, 's3');
                $newPhotos[] = \Storage::disk('s3')->url($path);
            }
        }

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            if (Arr::exists($data, 'title')) {
                $post->title = Arr::get($data, 'title');
            }
            if (Arr::exists($data, 'body')) {
                $post->body = Arr::get($data, 'body');
            }
            if (Arr::exists($data, 'type')) {
                $post->type = Arr::get($data, 'type');
            }
            if (Arr::exists($data, 'photos')) {
                $photos = Arr::get($data, 'photos');
                if (empty($photos)) {
                    $photos = [];
                } else if (is_array($photos)) {
                    $photos = collect($photos)->filter(function ($photo) {
                        return !empty($photo);
                    })->values()
                    ->toArray();
                }
                $post->photos = $photos;
            }
            if (count($newPhotos) > 0) {
                $post->photos = array_merge(
                    $post->photos->toArray(),
                    $newPhotos
                );
            }

            $post->save();

            $post->load(Post::DEFAULT_RELATIONS);

            \DB::commit();

            return new PostResource($post);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
