<?php

namespace App\Http\Controllers\Project\Post;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function __invoke($projectUuid, $postUuid)
    {
        $project = find_project($projectUuid);
        abort_if(is_null($project), 404, 'Not found');

        $post = Post::with(['user'])
            ->where('uuid', $postUuid)
            ->where('tenant_id', tenant('id'))
            ->where('project_id', $project->id)
            ->first();
        abort_if(is_null($post), 404, 'Not found');

        return new PostResource($post);
    }
}
