<?php

namespace App\Http\Controllers\Project\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\Post\StoreRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request, $projectUuid)
    {
        $project = find_project($projectUuid);
        abort_if(is_null($project), 404, 'Not found');
        abort_if($project->visible, 409, 'Project already published');

        $images = $request->file('images');
        $photos = [];
        if (!empty($images)) {
            foreach ($images as $file) {
                $extension = $file->guessExtension();

                $basePath = tenant('uuid') . '/' . date('Ym');
                $filename = date('Ymd-His-') . uniqid() . '.' . $extension;

                $path = $file->storeAs($basePath, $filename, 's3');
                $photos[] = \Storage::disk('s3')->url($path);
            }
        }

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $post = Post::create([
                'project_id' => $project->id,
                'user_id' => auth()->user()->id,
                'tenant_id' => tenant('id'),
                'uuid' => Post::generateUUID(),
                'title' => Arr::get($data, 'title'),
                'body' => Arr::get($data, 'body'),
                'type' => Arr::get($data, 'type'),
                'photos' => $photos,
            ]);

            $post->load(Post::DEFAULT_RELATIONS);

            \DB::commit();

            return new PostResource($post);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
