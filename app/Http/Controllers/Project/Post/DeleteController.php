<?php

namespace App\Http\Controllers\Project\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke($projectUuid, $postUuid)
    {
        $project = find_project($projectUuid);
        abort_if(is_null($project), 404, 'Not found');
        abort_if($project->visible, 409, 'Project already published');

        $query = Post::where('uuid', $postUuid)
            ->where('tenant_id', tenant('id'))
            ->where('project_id', $project->id);
        if (!is_admin()) {
            $query = $query->where('user_id', auth()->user()->id);
        }
        $post = $query->first();
        abort_if(is_null($post), 404, 'Not found');

        try {
            \DB::beginTransaction();

            $post->delete();

            \DB::commit();

            return response()
                ->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
