<?php

namespace App\Http\Controllers\Project\Certificate;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\Certificate\StoreRequest;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request, $projectUuid)
    {
        $project = find_project($projectUuid);
        abort_if(is_null($project), 404, 'Not found');

        $project->load([
            'team',
            'team.users',
            'team.workshop',
        ]);

        $data = $request->validated();

        $template = Arr::get($data, 'template');
        $parameters = Arr::get($data, 'parameters', []);

        try {
            \DB::beginTransaction();

            $project->certificate_url = $this->render($project, $template, $parameters);
            $project->certificate_created_at = now();
            $project->save();

            \DB::commit();

            return new ProjectResource($project);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }

    public function render($project, $template, $parameters)
    {
        $team = $project->team;
        $users = (!is_null($team))
            ? $team->users
            : collect();
        $workshop = (!is_null($team) && !is_null($team->workshop))
            ? $team->workshop
            : null;

        $content = view("projects.{$template}", [
            'parameters' => $parameters,
            'project' => $project,
            'team' => $team,
            'users' => $users,
            'workshop' => $workshop,
        ])->render();

        $pdf = pdf();
        $pdf->writeHTML($content);
        $pdfContent = $pdf->Output('document.pdf', 'S');

        $path = tenant('uuid')
            . '/certificates/'
            . date('Ym')
            . date('Ymd-His-')
            . uniqid()
            . '.pdf';

        \Storage::disk('s3')->put($path, $pdfContent);

        return \Storage::disk('s3')->url($path);
    }
}
