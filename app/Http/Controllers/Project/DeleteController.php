<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke($uuid)
    {
        $user = auth()->user();

        $project = Project::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->first();

        abort_if(is_null($project), 404, 'Not found');

        try {
            \DB::beginTransaction();

            $project->delete();

            \DB::commit();

            return response()
                ->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
