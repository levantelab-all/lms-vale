<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\UpdateRequest;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, $uuid)
    {
        $project = find_project($uuid);

        abort_if(is_null($project), 404, 'Not found');

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            if (Arr::exists($data, 'title')) {
                $project->title = Arr::get($data, 'title');
            }
            if (Arr::exists($data, 'description')) {
                $project->description = Arr::get($data, 'description');
            }
            if (Arr::exists($data, 'cover_url')) {
                $project->cover_url = Arr::get($data, 'cover_url');
            }
            if (Arr::exists($data, 'visible') && is_admin()) {
                $project->visible = Arr::get($data, 'visible');
            }
            $project->properties = array_merge(
                (is_null($project->properties)) ? [] : $project->properties->toArray(),
                $request->except(Project::PROTECTED_FIELDS)
            );
            $project->save();

            $project->load(Project::DEFAULT_RELATIONS);

            \DB::commit();

            return new ProjectResource($project);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
