<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\StoreRequest;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $project = Project::create([
                'team_id' => Arr::get($data, 'team_id'),
                'tenant_id' => tenant('id'),
                'uuid' => Project::generateUUID(),
                'visible' => false,
            ]);

            $project->load(Project::DEFAULT_RELATIONS);

            \DB::commit();

            return new ProjectResource($project);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
