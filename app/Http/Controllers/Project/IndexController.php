<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(PaginationRequest $request)
    {
        $user = auth()->user();

        $pageSize = (int) request('size', 20);

        $query = Project::where('tenant_id', tenant('id'));

        if (!is_admin($user)) {
            $user->load('teams');

            $teamIds = $user->teams->pluck('id');

            $query = $query->where(function ($q) use ($teamIds) {
                $q->where('visible', true)
                    ->orWhereIn('team_id', $teamIds);
            });
        }

        $queryBuilder = QueryBuilder::for($query)
            ->allowedIncludes([
                'team',
                'team.users',
                'team.workshop',
                'posts',
            ])->allowedFilters([
                AllowedFilter::exact('id', 'uuid'),
                AllowedFilter::partial('title'),
                AllowedFilter::partial('description'),
                AllowedFilter::exact('visible'),
                AllowedFilter::scope('states'),
            ])->allowedSorts([
                AllowedSort::field('title'),
                AllowedSort::field('created_at'),
                AllowedSort::field('updated_at'),
            ])->defaultSort('-created_at');

        if ($pageSize > 0) {
            $projects = $queryBuilder->paginate($pageSize);

            return ProjectResource::collection($projects);
        } else {
            $projects = $queryBuilder->get();

            return ProjectResource::collection($projects)
                ->additional([
                    'meta' => [
                        'total' => $projects->count(),
                    ],
                ]);
        }
    }
}
