<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ShowController extends Controller
{
    public function __invoke($uuid)
    {
        $query = find_project_query($uuid);

        $project = QueryBuilder::for($query)
            ->allowedIncludes([
                'team',
                'team.users',
                'team.workshop',
                'posts',
            ])->first();

        abort_if(is_null($project), 404, 'Not found');

        return new ProjectResource($project);
    }
}
