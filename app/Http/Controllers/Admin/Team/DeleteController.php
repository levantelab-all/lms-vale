<?php

namespace App\Http\Controllers\Admin\Team;

use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke($uuid)
    {
        $team = Team::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->first();

        abort_if(is_null($team), 404, 'Not found');

        try {
            \DB::beginTransaction();

            $team->delete();

            \DB::commit();

            return response()
                ->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
