<?php

namespace App\Http\Controllers\Admin\Team;

use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ShowController extends Controller
{
    public function __invoke($uuid)
    {
        $query = Team::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'));

        $team = QueryBuilder::for($query)
            ->allowedIncludes([
                'users',
                'workshop',
                'projects',
            ])->first();

        abort_if(is_null($team), 404, 'Not found');

        return new TeamResource($team);
    }
}
