<?php

namespace App\Http\Controllers\Admin\Team;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Team\UpdateRequest;
use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, $uuid)
    {
        $team = Team::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->first();

        abort_if(is_null($team), 404, 'Not found');

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            if (Arr::exists($data, 'name')) {
                $team->name = Arr::get($data, 'name');
            }
            if (Arr::exists($data, 'ref_id')) {
                $team->ref_id = Arr::get($data, 'ref_id', null);
            }
            $team->properties = array_merge(
                (is_null($team->properties)) ? [] : $team->properties->toArray(),
                $request->except(Team::PROTECTED_FIELDS)
            );

            $team->save();

            if (Arr::exists($data, 'users')) {
                $team->users()->sync(Arr::get($data, 'users', []));
            }

            $team->load(Team::DEFAULT_RELATIONS);

            \DB::commit();

            return new TeamResource($team);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
