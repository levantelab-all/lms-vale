<?php

namespace App\Http\Controllers\Admin\Team;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(PaginationRequest $request)
    {
        $pageSize = (int) request('size', 20);

        $query = Team::where('tenant_id', tenant('id'));

        $queryBuilder = QueryBuilder::for($query)
            ->allowedIncludes([
                'users',
                'workshop',
                'projects',
            ])->allowedFilters([
                AllowedFilter::exact('id', 'uuid'),
                AllowedFilter::partial('name'),
                AllowedFilter::scope('ref'),
                AllowedFilter::scope('search'),
                AllowedFilter::scope('states'),
            ])->allowedSorts([
                AllowedSort::field('name'),
                AllowedSort::field('created_at'),
                AllowedSort::field('updated_at'),
            ])->defaultSort('name');

        if ($pageSize > 0) {
            $teams = $queryBuilder->paginate($pageSize);

            return TeamResource::collection($teams);
        } else {
            $teams = $queryBuilder->get();

            return TeamResource::collection($teams)
                ->additional([
                    'meta' => [
                        'total' => $teams->count(),
                    ],
                ]);
        }
    }
}
