<?php

namespace App\Http\Controllers\Admin\Team;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Team\StoreRequest;
use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $team = Team::create([
                'tenant_id' => tenant('id'),
                'uuid' => Team::generateUUID(),
                'name' => Arr::get($data, 'name'),
                'ref_id' => Arr::get($data, 'ref_id', null),
                'properties' => $request->except(Team::PROTECTED_FIELDS),
            ]);

            if (Arr::exists($data, 'users')) {
                $team->users()->sync(Arr::get($data, 'users', []));
            }

            $team->load(Team::DEFAULT_RELATIONS);

            \DB::commit();

            return new TeamResource($team);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
