<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ShowController extends Controller
{
    public function __invoke($uuid)
    {
        $query = User::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'));

        $user = QueryBuilder::for($query)
            ->allowedIncludes([
                'teams',
                'teams.workshop',
                'teams.projects',
                'certificates',
            ])->first();

        abort_if(is_null($user), 404, 'Not found');

        return new UserResource($user);
    }
}
