<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(PaginationRequest $request)
    {
        $pageSize = (int) request('size', 20);

        $query = User::where('tenant_id', tenant('id'));

        $queryBuilder = QueryBuilder::for($query)
            ->allowedIncludes([
                'teams',
                'teams.workshop',
                'teams.projects',
                'certificates',
            ])->allowedFilters([
                AllowedFilter::exact('id', 'uuid'),
                AllowedFilter::partial('name'),
                AllowedFilter::partial('email'),
                AllowedFilter::partial('phone'),
                AllowedFilter::exact('cpf'),
                AllowedFilter::scope('active'),
                AllowedFilter::exact('level'),
                AllowedFilter::scope('search'),
                AllowedFilter::exact('allow_email_messages', 'properties->allow_email_messages'),
            ])->allowedSorts([
                AllowedSort::field('name'),
                AllowedSort::field('created_at'),
                AllowedSort::field('updated_at'),
            ])->defaultSort('name');

        if ($pageSize > 0) {
            $users = $queryBuilder->paginate($pageSize);

            return UserResource::collection($users);
        } else {
            $users = $queryBuilder->get();

            return UserResource::collection($users)
                ->additional([
                    'meta' => [
                        'total' => $users->count(),
                    ],
                ]);
        }
    }
}
