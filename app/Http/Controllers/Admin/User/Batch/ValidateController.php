<?php

namespace App\Http\Controllers\Admin\User\Batch;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Batch\ValidateRequest;
use App\Http\Resources\UserImportResource;
use App\Imports\UsersBatchImport;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ValidateController extends Controller
{
    public function __invoke(ValidateRequest $request)
    {
        $file = $request->file('file');

        $sheets = \Excel::toCollection(new UsersBatchImport(), $file);
        $users = $sheets->first();

        $users = $users->map(function ($user) {
            return $user->toArray();
        })->filter(function ($user) {
            return validate($user, [
                'name' => 'required|string|max:191',
                'cpf' => [
                    'required',
                    'string',
                    'max:191',
                    'cpf',
                    Rule::unique('users', 'cpf')->where(function ($query) {
                        return $query->where('tenant_id', tenant('id'));
                    })
                ],
                'phone' => 'nullable|string|max:191',
                'email' => [
                    'required',
                    'string',
                    'max:191',
                    'email',
                    Rule::unique('users', 'email')->where(function ($query) {
                        return $query->where('tenant_id', tenant('id'));
                    })
                ],
            ]);
        })->unique('cpf')
        ->unique('email')
        ->sortBy('name');

        return UserImportResource::collection($users)
            ->additional([
                'meta' => [
                    'total' => $users->count(),
                ],
            ]);
    }
}
