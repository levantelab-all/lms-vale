<?php

namespace App\Http\Controllers\Admin\User\Batch;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Batch\ImportRequest;
use App\Models\User;
use App\Notifications\FinishSignupNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ImportController extends Controller
{
    public function __invoke(ImportRequest $request)
    {
        $users = $request->input('users');

        $tenant = tenant();

        foreach($users as $model) {
            if (empty($model['name'])) {
                continue;
            }

            try {
                \DB::beginTransaction();

                $password = Str::upper(Str::random());

                $phone = clean_phone($model['phone']);
                if (empty($phone)) {
                    $phone = null;
                }

                $user = User::create([
                    'tenant_id' => $tenant->id,
                    'uuid' => User::generateUUID(),
                    'name' => $model['name'],
                    'email' => trim($model['email']),
                    'phone' => $phone,
                    'cpf' => clean_cpf($model['cpf']),
                    'password' => bcrypt($password),
                    'level' => User::LEVEL_USER,
                ]);

                $user->notify((new FinishSignupNotification(
                    $tenant,
                    $password
                ))->afterCommit());

                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollback();

                \Log::error('User import ' . var_export($model, true) . ' failed!', [
                    'exception' => $e,
                ]);
            }
        }

        return response()
            ->noContent();
    }
}
