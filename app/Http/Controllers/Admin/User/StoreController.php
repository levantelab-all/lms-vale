<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\StoreRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Notifications\FinishSignupNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $password = Str::upper(Str::random());

            $user = User::create([
                'tenant_id' => tenant('id'),
                'uuid' => User::generateUUID(),
                'name' => Arr::get($data, 'name'),
                'email' => trim(Arr::get($data, 'email')),
                'phone' => clean_phone(Arr::get($data, 'phone')),
                'cpf' => clean_cpf(Arr::get($data, 'cpf')),
                'password' => bcrypt($password),
                'level' => Arr::get($data, 'level'),
                'properties' => $request->except(User::PROTECTED_FIELDS),
            ]);

            if (Arr::exists($data, 'teams')) {
                $user->teams()->sync(Arr::get($data, 'teams', []));
            }

            $user->load(User::DEFAULT_RELATIONS);

            $user->notify((new FinishSignupNotification(
                tenant(),
                $password
            ))->afterCommit());

            \DB::commit();

            return new UserResource($user);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
