<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, $uuid)
    {
        $user = User::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->first();

        abort_if(is_null($user), 404, 'Not found');

        $data = $request->validated();
        abort_if(!Arr::exists($data, 'level') && $user->level === User::LEVEL_DELETED, 404, 'Not found');

        try {
            \DB::beginTransaction();

            if (Arr::exists($data, 'name')) {
                $user->name = Arr::get($data, 'name');
            }
            if (Arr::exists($data, 'email')) {
                $user->email = trim(Arr::get($data, 'email'));
            }
            if (Arr::exists($data, 'phone')) {
                $user->phone = clean_phone(Arr::get($data, 'phone'));
            }
            if (Arr::exists($data, 'cpf')) {
                $user->cpf = clean_cpf(Arr::get($data, 'cpf'));
            }
            if (Arr::exists($data, 'level')) {
                $user->level = Arr::get($data, 'level');
            }
            $user->properties = array_merge(
                (is_null($user->properties)) ? [] : $user->properties->toArray(),
                $request->except(User::PROTECTED_FIELDS)
            );
            $user->save();

            if (Arr::exists($data, 'teams')) {
                $user->teams()->sync(Arr::get($data, 'teams', []));
            }

            $user->load(User::DEFAULT_RELATIONS);

            \DB::commit();

            return new UserResource($user);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
