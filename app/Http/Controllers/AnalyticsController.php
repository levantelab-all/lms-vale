<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnalyticsRequest;
use App\Models\Analytic;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    public function __invoke(AnalyticsRequest $request)
    {
        $data = $request->validated();

        try {
            \DB::beginTransaction();

            $data['tenant_id'] = tenant('id');
            $data['user_id'] = auth()->user()->id;
            Analytic::create($data);

            \DB::commit();

            return response()->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
