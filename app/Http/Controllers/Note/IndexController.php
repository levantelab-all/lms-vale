<?php

namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\NoteResource;
use App\Models\Note;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(PaginationRequest $request)
    {
        $pageSize = (int) request('size', 20);

        $query = Note::where('tenant_id', tenant('id'))
            ->where('user_id', auth()->user()->id);

        $queryBuilder = QueryBuilder::for($query)
            ->allowedFilters([
                AllowedFilter::exact('id', 'uuid'),
                AllowedFilter::partial('title'),
                AllowedFilter::partial('body'),
                AllowedFilter::scope('search'),
            ])->allowedSorts([
                AllowedSort::field('title'),
                AllowedSort::field('created_at'),
                AllowedSort::field('updated_at'),
            ])->defaultSort('-created_at');

        if ($pageSize > 0) {
            $teams = $queryBuilder->paginate($pageSize);

            return NoteResource::collection($teams);
        } else {
            $teams = $queryBuilder->get();

            return NoteResource::collection($teams)
                ->additional([
                    'meta' => [
                        'total' => $teams->count(),
                    ],
                ]);
        }
    }
}
