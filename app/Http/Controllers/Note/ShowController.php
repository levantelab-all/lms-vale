<?php

namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use App\Http\Resources\NoteResource;
use App\Models\Note;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ShowController extends Controller
{
    public function __invoke($uuid)
    {
        $query = Note::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->where('user_id', auth()->user()->id);

        $note = QueryBuilder::for($query)->first();

        abort_if(is_null($note), 404, 'Not found');

        return new NoteResource($note);
    }
}
