<?php

namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use App\Models\Note;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke($uuid)
    {
        $note = Note::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->where('user_id', auth()->user()->id)
            ->first();

        abort_if(is_null($note), 404, 'Not found');

        try {
            \DB::beginTransaction();

            $note->delete();

            \DB::commit();

            return response()
                ->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
