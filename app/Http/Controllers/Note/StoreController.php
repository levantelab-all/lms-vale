<?php

namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequest;
use App\Http\Resources\NoteResource;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(NoteRequest $request)
    {
        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $note = Note::create([
                'user_id' => auth()->user()->id,
                'tenant_id' => tenant('id'),
                'uuid' => Note::generateUUID(),
                'title' => Arr::get($data, 'title'),
                'body' => Arr::get($data, 'body'),
                'properties' => $request->except(Note::PROTECTED_FIELDS),
            ]);

            $note->load(Note::DEFAULT_RELATIONS);

            \DB::commit();

            return new NoteResource($note);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
