<?php

namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequest;
use App\Http\Resources\NoteResource;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UpdateController extends Controller
{
    public function __invoke(NoteRequest $request, $uuid)
    {
        $note = Note::where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->where('user_id', auth()->user()->id)
            ->first();

        abort_if(is_null($note), 404, 'Not found');

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            if (Arr::exists($data, 'title')) {
                $note->title = Arr::get($data, 'title');
            }
            if (Arr::exists($data, 'body')) {
                $note->body = Arr::get($data, 'body');
            }
            $note->properties = array_merge(
                (is_null($note->properties)) ? [] : $note->properties->toArray(),
                $request->except(Note::PROTECTED_FIELDS)
            );

            $note->save();

            $note->load(Note::DEFAULT_RELATIONS);

            \DB::commit();

            return new NoteResource($note);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
