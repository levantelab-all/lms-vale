<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PasswordResetLinkRequest;
use App\Http\Resources\UserResetResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class PasswordResetLinkController extends Controller
{
    public function __invoke(PasswordResetLinkRequest $request)
    {
        $credentials = [
            'tenant_id' => tenant('id'),
            'level' => [
                User::LEVEL_ADMIN,
                User::LEVEL_USER,
            ],
        ];

        $username = $request->input('username');
        if (validate($username, 'email')) {
            $credentials['email'] = trim($username);
        } else if (validate($username, 'cpf')) {
            $credentials['cpf'] = clean_cpf($username);
        } else {
            throw ValidationException::withMessages([
                'username' => 'The username field should contain a valid email or CPF number'
            ]);
        }

        $broker = Password::broker(config('auth.default.passwords'));

        $resetedUser = null;

        $status = $broker->sendResetLink($credentials, function ($user, $token) use (&$resetedUser) {
            $user->sendPasswordResetNotification($token);

            $resetedUser = $user;
        });

        return $status == Password::RESET_LINK_SENT
            ? new UserResetResource($resetedUser)
            : throw ValidationException::withMessages([
                    'username' => [trans($status)],
                ]);
    }
}
