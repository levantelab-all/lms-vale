<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\NewPasswordRequest;
use App\Http\Resources\UserWithTokenResource;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class NewPasswordController extends Controller
{
    public function __invoke(NewPasswordRequest $request)
    {
        $credentials = [
            'tenant_id' => tenant('id'),
            'token' => $request->input('token'),
            'level' => [
                User::LEVEL_ADMIN,
                User::LEVEL_USER,
            ],
            'password' => $request->input('password'),
        ];

        $username = $request->input('username');
        if (validate($username, 'email')) {
            $credentials['email'] = trim($username);
        } else if (validate($username, 'cpf')) {
            $credentials['cpf'] = clean_cpf($username);
        } else {
            throw ValidationException::withMessages([
                'username' => 'The username field should contain a valid email or CPF number'
            ]);
        }

        $broker = Password::broker(config('auth.default.passwords'));

        $resetedUser = null;

        $status = $broker->reset($credentials, function ($user) use ($request, &$resetedUser) {
            $user->password = bcrypt($request['password']);
            $user->remember_token = Str::random(60);
            $user->save();

            event(new PasswordReset($user));

            $resetedUser = $user;
        });

        if ($status == Password::PASSWORD_RESET) {
            $resetedUser->load(User::DEFAULT_RELATIONS);

            $token = auth()->login($resetedUser);
            $expiresIn = auth()->factory()->getTTL() * 60;

            return new UserWithTokenResource($resetedUser, $token, $expiresIn);
        } else {
            throw ValidationException::withMessages([
                'email' => [trans($status)],
            ]);
        }
    }
}
