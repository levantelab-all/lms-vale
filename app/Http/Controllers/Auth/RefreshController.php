<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserWithTokenResource;
use App\Models\User;
use Illuminate\Http\Request;

class RefreshController extends Controller
{
    public function __invoke()
    {
        $token = auth()->refresh();

        $user = auth()->user();
        $expiresIn = auth()->factory()->getTTL() * 60;

        $user->load(User::DEFAULT_RELATIONS);

        return new UserWithTokenResource($user, $token, $expiresIn);
    }
}
