<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SignupRequest;
use App\Http\Resources\UserWithTokenResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class SignupController extends Controller
{
    public function __invoke(SignupRequest $request)
    {
        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $user = User::create([
                'tenant_id' => tenant('id'),
                'uuid' => User::generateUUID(),
                'name' => Arr::get($data, 'name'),
                'email' => trim(Arr::get($data, 'email')),
                'phone' => clean_phone(Arr::get($data, 'phone')),
                'cpf' => clean_cpf(Arr::get($data, 'cpf')),
                'password' => bcrypt(Arr::get($data, 'password')),
                'level' => User::LEVEL_USER,
                'properties' => $request->except(User::PROTECTED_FIELDS),
            ]);

            if (Arr::exists($data, 'teams')) {
                $user->teams()->sync(Arr::get($data, 'teams', []));
            }

            $user->load(User::DEFAULT_RELATIONS);

            $token = auth()->login($user);
            $expiresIn = auth()->factory()->getTTL() * 60;

            \DB::commit();

            return new UserWithTokenResource($user, $token, $expiresIn);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
