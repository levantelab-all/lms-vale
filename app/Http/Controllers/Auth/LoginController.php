<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\UserWithTokenResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request)
    {
        $credentials = [
            'password' => $request->input('password'),
            'tenant_id' => tenant('id'),
            'level' => [
                User::LEVEL_ADMIN,
                User::LEVEL_USER,
            ],
        ];

        $username = $request->input('username');
        if (validate($username, 'email')) {
            $credentials['email'] = trim($username);
        } else if (validate($username, 'cpf')) {
            $credentials['cpf'] = clean_cpf($username);
        } else {
            throw ValidationException::withMessages([
                'username' => 'The username field should contain a valid email or CPF number'
            ]);
        }

        if (! $token = auth()->claims(['iss' => tenant('uuid')])->attempt($credentials)) {
            abort(401, 'Not authenticated');
        }

        $user = auth()->user();
        $expiresIn = auth()->factory()->getTTL() * 60;

        $user->load(User::DEFAULT_RELATIONS);

        return new UserWithTokenResource($user, $token, $expiresIn);
    }
}
