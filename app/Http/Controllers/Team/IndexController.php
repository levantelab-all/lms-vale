<?php

namespace App\Http\Controllers\Team;

use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = auth()->user();

        $query = $user->teams();

        $teams = QueryBuilder::for($query)
            ->allowedIncludes([
                'users',
                'workshop',
                'projects',
            ])->allowedFilters([
                AllowedFilter::exact('id', 'uuid'),
                AllowedFilter::partial('name'),
                AllowedFilter::scope('ref'),
            ])->allowedSorts([
                AllowedSort::field('name'),
                AllowedSort::field('created_at'),
                AllowedSort::field('updated_at'),
            ])->defaultSort('name')
            ->paginate(20);

        return TeamResource::collection($teams);
    }
}
