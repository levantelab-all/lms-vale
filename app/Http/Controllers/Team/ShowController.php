<?php

namespace App\Http\Controllers\Team;

use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ShowController extends Controller
{
    public function __invoke(Request $request, $uuid)
    {
        $user = auth()->user();

        $query = $user->teams()
            ->where('teams.uuid', $uuid);

        $team = QueryBuilder::for($query)
            ->allowedIncludes([
                'users',
                'workshop',
                'projects',
            ])->first();

        abort_if(is_null($team), 404, 'Not found');

        return new TeamResource($team);
    }
}
