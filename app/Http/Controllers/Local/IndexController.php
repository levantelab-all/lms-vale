<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\ContentResource;
use App\Models\Content;
use App\Pipeline\ReturnResources;
use App\Pipeline\SetupConditions;
use App\Pipeline\SetupFilters;
use App\Pipeline\SetupIncludes;
use App\Pipeline\SetupOrders;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke($type, PaginationRequest $request)
    {
        authorize_read_resource('local', $type);

        content_setup_relationships('local', $type);

        $config = config("local.{$type}");

        $query = Content::where('type', $type)
            ->where('tenant_id', tenant('id'))
            ->where(function ($q) {
                $user = auth()->user();
                $userId = (!is_null($user))
                    ? $user->id
                    : 0;

                $q->where('owner_id', $userId);

                if (is_admin()) {
                    $q->orWhere('private', 0);
                } else {
                    $q->orWhere(function ($q2) {
                        $q2->where('private', 0)
                            ->where('visible', 1);
                    });
                }
            });

        $queryBuilder = QueryBuilder::for($query);

        return app(Pipeline::class)
            ->send(compact('config', 'queryBuilder'))
            ->through([
                SetupConditions::class,
                SetupFilters::class,
                SetupIncludes::class,
                SetupOrders::class,
                ReturnResources::class,
            ])
            ->thenReturn();
    }
}
