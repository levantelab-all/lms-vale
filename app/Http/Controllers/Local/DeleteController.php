<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke($type, $uuid, Request $request)
    {
        authorize_write_resource('local', $type);

        $content = Content::where('type', $type)
            ->where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->first();

        abort_if(is_null($content), 404, 'Not found');
        if (is_admin()) {
            abort_if($content->private && $content->owner_id != auth()->user()->id, 403, 'Not authorized');
        } else {
            abort_if($content->owner_id != auth()->user()->id, 403, 'Not authorized');
        }

        try {
            \DB::beginTransaction();

            $content->delete();

            \DB::commit();

            return response()->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
