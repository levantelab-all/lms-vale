<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateLocalRequest;
use App\Http\Resources\ContentResource;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UpdateController extends Controller
{
    public function __invoke($type, $uuid, UpdateLocalRequest $request)
    {
        authorize_write_resource('local', $type);

        $content = Content::where('type', $type)
            ->where('uuid', $uuid)
            ->where('tenant_id', tenant('id'))
            ->first();

        abort_if(is_null($content), 404, 'Not found');
        if (is_admin()) {
            abort_if($content->private && $content->owner_id != auth()->user()->id, 403, 'Not authorized');
        } else {
            abort_if($content->owner_id != auth()->user()->id, 403, 'Not authorized');
        }

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $content->ref_id = Arr::get($data, 'ref_id', $content->ref_id);
            $content->parent_id = Arr::get($data, 'parent_id', $content->parent_id);
            $content->name = Arr::get($data, 'name', $content->name);
            $content->private = (boolean) Arr::get($data, 'private', $content->private);
            $content->visible = (boolean) Arr::get($data, 'visible', $content->visible);
            $content->order = (int) Arr::get($data, 'order', $content->order);

            $content->properties = array_merge(
                $content->properties->toArray(),
                Arr::except($data, Content::_DIRECT_FIELDS)
            );

            $content->save();

            \DB::commit();

            return new ContentResource($content);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
