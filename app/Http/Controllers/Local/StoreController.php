<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLocalRequest;
use App\Http\Resources\ContentResource;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke($type, CreateLocalRequest $request)
    {
        authorize_write_resource('local', $type);

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            $content = Content::create([
                'tenant_id' => tenant('id'),
                'type' => $type,
                'uuid' => Content::generateUUID($type),
                'ref_id' => Arr::get($data, 'ref_id', null),
                'parent_id' => Arr::get($data, 'parent_id', null),
                'owner_id' => auth()->user()->id,
                'name' => Arr::get($data, 'name'),
                'properties' => Arr::except($data, Content::_DIRECT_FIELDS),
                'private' => (boolean) Arr::get($data, 'private', false),
                'visible' => (boolean) Arr::get($data, 'visible', true),
                'order' => (int) Arr::get($data, 'order', 0),
            ]);

            \DB::commit();

            return new ContentResource($content);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
