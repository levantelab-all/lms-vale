<?php

namespace App\Http\Controllers\Variable;

use App\Http\Controllers\Controller;
use App\Http\Resources\VariableResource;
use App\Models\Variable;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function __invoke($group, $key)
    {
        $variable = Variable::where([
            'tenant_id' => tenant('id'),
            'owner_id' => auth()->user()->id,
            'group' => $group,
            'key' => $key,
        ])->first();

        abort_if(is_null($variable), 404, 'Not found');

        return new VariableResource($variable);
    }
}
