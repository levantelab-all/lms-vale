<?php

namespace App\Http\Controllers\Variable;

use App\Http\Controllers\Controller;
use App\Models\Variable;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke($group, $key)
    {
        try {
            \DB::beginTransaction();

            $variable = Variable::where([
                'tenant_id' => tenant('id'),
                'owner_id' => auth()->user()->id,
                'group' => $group,
                'key' => $key,
            ])->first();

            abort_if(is_null($variable), 404, 'Not found');

            $variable->delete();

            \DB::commit();

            return response()->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
