<?php

namespace App\Http\Controllers\Variable\Batch;

use App\Http\Controllers\Controller;
use App\Models\Variable;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DeleteController extends Controller
{
    public function __invoke($group, Request $request)
    {
        try {
            \DB::beginTransaction();

            $keys = Arr::wrap($request->input('keys', []));

            if (empty($keys)) {
                Variable::where('tenant_id', tenant('id'))
                    ->where('owner_id', auth()->user()->id)
                    ->where('group', $group)
                    ->delete();
            } else {
                Variable::where('tenant_id', tenant('id'))
                    ->where('owner_id', auth()->user()->id)
                    ->where('group', $group)
                    ->whereIn('key', $keys)
                    ->delete();
            }

            \DB::commit();

            return response()->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
