<?php

namespace App\Http\Controllers\Variable\Batch;

use App\Http\Controllers\Controller;
use App\Http\Resources\VariableResource;
use App\Models\Variable;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __invoke($group, Request $request)
    {
        $variables = collect();

        try {
            \DB::beginTransaction();

            foreach($request->all() as $key => $value) {
                $variable = Variable::firstOrNew([
                    'tenant_id' => tenant('id'),
                    'owner_id' => auth()->user()->id,
                    'group' => $group,
                    'key' => $key,
                ]);

                $variable->value = (is_array($value))
                    ? json_encode($value)
                    : $value;

                $variable->save();

                $variables->push($variable);
            }

            \DB::commit();

            return VariableResource::collection($variables);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
