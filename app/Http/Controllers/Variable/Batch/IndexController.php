<?php

namespace App\Http\Controllers\Variable\Batch;

use App\Http\Controllers\Controller;
use App\Http\Resources\VariableResource;
use App\Models\Variable;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke($group)
    {
        $variables = Variable::where([
            'tenant_id' => tenant('id'),
            'owner_id' => auth()->user()->id,
            'group' => $group,
        ])->get();

        return VariableResource::collection($variables);
    }
}
