<?php

namespace App\Http\Controllers\Variable;

use App\Http\Controllers\Controller;
use App\Http\Resources\VariableResource;
use App\Models\Variable;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __invoke($group, $key, Request $request)
    {
        $value = $request->input('value');

        if (is_array($value) || is_object($value)) {
            $value = json_encode($value);
        }

        try {
            \DB::beginTransaction();

            $variable = Variable::firstOrNew([
                'tenant_id' => tenant('id'),
                'owner_id' => auth()->user()->id,
                'group' => $group,
                'key' => $key,
            ]);
            $variable->value = $value;
            $variable->save();

            \DB::commit();

            return new VariableResource($variable);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
