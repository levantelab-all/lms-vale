<?php

namespace App\Http\Controllers;

use App\Http\Resources\SimpleAdminResource;
use App\Models\User;
use Illuminate\Http\Request;

class AdminUsersController extends Controller
{
    public function __invoke()
    {
        $users = User::where('tenant_id', tenant('id'))
            ->where('level', User::LEVEL_ADMIN)
            ->orderBy('name')
            ->get();

        return SimpleAdminResource::collection($users);
    }
}
