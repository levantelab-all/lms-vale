<?php

namespace App\Http\Controllers\Certificate;

use App\Http\Controllers\Controller;
use App\Http\Requests\Certificate\StoreRequest;
use App\Http\Resources\CertificateResource;
use App\Models\Certificate;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $user = auth()->user();

        $data = $request->validated();
        $refId = Arr::get($data, 'ref_id');

        try {
            \DB::beginTransaction();

            $certificate = Certificate::firstOrNew([
                'tenant_id' => tenant('id'),
                'user_id' => $user->id,
                'ref_id' => $refId,
            ]);

            $certificate->uuid = Certificate::generateUUID();
            $certificate->template = Arr::get($data, 'template');
            $certificate->parameters = Arr::get($data, 'parameters');
            $certificate->url = $certificate->render();
            $certificate->save();

            \DB::commit();

            return new CertificateResource($certificate);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
