<?php

namespace App\Http\Controllers\Certificate;

use App\Http\Controllers\Controller;
use App\Http\Resources\CertificateResource;
use App\Models\Certificate;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function __invoke($uuid)
    {
        $certificate = Certificate::where('tenant_id', tenant('id'))
            ->where('uuid', $uuid)
            ->first();

        abort_if(is_null($certificate), 404, 'Not found');

        return new CertificateResource($certificate);
    }
}
