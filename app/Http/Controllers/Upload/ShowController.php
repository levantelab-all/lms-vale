<?php

namespace App\Http\Controllers\Upload;

use App\Http\Controllers\Controller;
use App\Http\Resources\UploadResource;
use App\Models\Upload;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function __invoke(Upload $upload)
    {
        abort_if($upload->tenant_id != tenant('id'), 404, 'Not found');

        return new UploadResource($upload);
    }
}
