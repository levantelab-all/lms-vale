<?php

namespace App\Http\Controllers\Upload;

use App\Http\Controllers\Controller;
use App\Models\Upload;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke(Upload $upload)
    {
        abort_if($upload->tenant_id != tenant('id'), 404, 'Not found');
        abort_if(!is_admin() && $upload->owner_id != auth()->user()->id, 403, 'Not authorized');

        try {
            \DB::beginTransaction();

            \Storage::disk('s3')->delete($upload->path);

            $upload->delete();

            \DB::commit();

            return response()->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
