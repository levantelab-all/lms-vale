<?php

namespace App\Http\Controllers\Upload;

use App\Http\Controllers\Controller;
use App\Http\Requests\UploadRequest;
use App\Http\Resources\UploadResource;
use App\Models\Upload;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __invoke(UploadRequest $request)
    {
        $file = $request->file('file');
        $extension = $file->guessExtension();

        $basePath = tenant('uuid') . '/' . date('Ym');
        $filename = date('Ymd-His-') . uniqid() . '.' . $extension;

        $path = $file->storeAs($basePath, $filename, 's3');
        $url = \Storage::disk('s3')->url($path);

        $name = $request->input('name');
        if (empty($name)) {
            $name = $file->getClientOriginalName();
        }

        try {
            \DB::beginTransaction();

            $upload = Upload::create([
                'tenant_id' => tenant('id'),
                'uuid' => Upload::generateUUID(),
                'owner_id' => auth()->user()->id,
                'name' => $name,
                'path' => $path,
                'url' => $url,
            ]);

            \DB::commit();

            return new UploadResource($upload);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
