<?php

namespace App\Http\Controllers\QualitySurvey;

use App\Http\Controllers\Controller;
use App\Http\Requests\QualitySurvey\StoreRequest;
use App\Models\QualitySurvey;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $user = auth()->user();

        $data = $request->validated();
        $refId = Arr::get($data, 'ref_id');

        try {
            \DB::beginTransaction();

            $survey = QualitySurvey::firstOrNew([
                'user_id' => $user->id,
                'ref_id' => $refId,
            ]);

            $survey->fill($data);
            $survey->user_id = $user->id;
            $survey->save();

            \DB::commit();

            return response()
                ->noContent();
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
