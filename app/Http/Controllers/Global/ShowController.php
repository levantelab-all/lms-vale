<?php

namespace App\Http\Controllers\Global;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContentResource;
use App\Models\Content;
use App\Pipeline\ReturnResource;
use App\Pipeline\SetupIncludes;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Spatie\QueryBuilder\QueryBuilder;

class ShowController extends Controller
{
    public function __invoke($type, $uuid, Request $request)
    {
        authorize_read_resource('global', $type);

        content_setup_relationships('global', $type);

        $config = config("global.{$type}");

        $queryBuilder = QueryBuilder::for(Content::where('type', $type)->where('uuid', $uuid)->whereNull('tenant_id'));

        return app(Pipeline::class)
            ->send(compact('config', 'queryBuilder'))
            ->through([
                SetupIncludes::class,
                ReturnResource::class,
            ])
            ->thenReturn();
    }
}
