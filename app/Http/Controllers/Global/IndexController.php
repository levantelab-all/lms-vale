<?php

namespace App\Http\Controllers\Global;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\ContentResource;
use App\Models\Content;
use App\Pipeline\ReturnResources;
use App\Pipeline\SetupFilters;
use App\Pipeline\SetupIncludes;
use App\Pipeline\SetupOrders;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Spatie\QueryBuilder\QueryBuilder;

class IndexController extends Controller
{
    public function __invoke($type, PaginationRequest $request)
    {
        authorize_read_resource('global', $type);

        content_setup_relationships('global', $type);

        $config = config("global.{$type}");

        $queryBuilder = QueryBuilder::for(Content::where('type', $type)->whereNull('tenant_id'));

        return app(Pipeline::class)
            ->send(compact('config', 'queryBuilder'))
            ->through([
                SetupFilters::class,
                SetupIncludes::class,
                SetupOrders::class,
                ReturnResources::class,
            ])
            ->thenReturn();
    }
}
