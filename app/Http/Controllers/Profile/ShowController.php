<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function __invoke()
    {
        $user = auth()->user();

        abort_if($user->tenant_id != tenant('id'), 403, 'Not authorized');

        $user->load(User::DEFAULT_RELATIONS);

        return new UserResource($user);
    }
}
