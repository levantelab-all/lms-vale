<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoreController extends Controller
{
    public function __invoke(ProfileRequest $request)
    {
        $user = auth()->user();

        abort_if($user->tenant_id != tenant('id'), 403, 'Not authorized');

        try {
            \DB::beginTransaction();

            $data = $request->validated();

            if (Arr::exists($data, 'name')) {
                $user->name = Arr::get($data, 'name');
            }
            if (Arr::exists($data, 'email')) {
                $user->email = trim(Arr::get($data, 'email'));
            }
            if (Arr::exists($data, 'phone')) {
                $user->phone = clean_phone(Arr::get($data, 'phone'));
            }
            if (Arr::exists($data, 'cpf')) {
                $user->cpf = clean_cpf(Arr::get($data, 'cpf'));
            }
            if (Arr::exists($data, 'password')) {
                $user->password = bcrypt(Arr::get($data, 'password'));
            }
            $user->properties = array_merge(
                (is_null($user->properties)) ? [] : $user->properties->toArray(),
                $request->except(User::PROTECTED_FIELDS)
            );
            $user->save();

            if (Arr::exists($data, 'teams')) {
                $user->teams()->sync(Arr::get($data, 'teams', []));
            }

            $user->load(User::DEFAULT_RELATIONS);

            \DB::commit();

            return new UserResource($user);
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}
