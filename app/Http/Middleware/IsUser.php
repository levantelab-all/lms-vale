<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsUser
{
    public function handle(Request $request, Closure $next)
    {
        abort_if(!is_user($request->user()), 403, 'Not authorized');

        return $next($request);
    }
}
