<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateResourceType
{
    public function handle(Request $request, Closure $next, $scope)
    {
        $type = $request->route('type');

        $types = config("{$scope}.types", []);

        abort_if(!in_array($type, $types), 412, 'Type not found');

        return $next($request);
    }
}
