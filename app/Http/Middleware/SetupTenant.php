<?php

namespace App\Http\Middleware;

use App\Models\Tenant;
use Closure;
use Illuminate\Http\Request;

class SetupTenant
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->hasHeader('X-Tenant')) {
            $tenants = config('tenants.tenants', []);
            $tenantId = $request->header('X-Tenant', null);

            if (!is_null($tenantId) && isset($tenants[$tenantId])) {
                $tenant = new Tenant;
                $tenant->id = $tenants[$tenantId]['id'];
                $tenant->uuid = $tenantId;
                $tenant->name = $tenants[$tenantId]['name'];
                $tenant->frontend_url = $tenants[$tenantId]['frontend_url'];

                app()->instance('tenant', $tenant);

                $user = auth()->user();
                abort_if(!is_null($user) && $user->tenant_id != $tenant->id, 403, 'Unauthorized');

                return $next($request);
            }
        }

        return $next($request);
    }
}
