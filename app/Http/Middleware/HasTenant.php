<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HasTenant
{
    public function handle(Request $request, Closure $next)
    {
        $tenant = tenant();
        abort_if(is_null($tenant), 417, 'Tenant not found');

        return $next($request);
    }
}
