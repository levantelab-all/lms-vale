<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    public function handle(Request $request, Closure $next)
    {
        abort_if(!is_admin($request->user()), 403, 'Not authorized');

        return $next($request);
    }
}
