<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class NewPasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'token' => 'required',
            'username' => 'required',
            // 'email' => 'required|email',
            'password' => [
                'required',
                'confirmed',
                Password::min(8)->uncompromised(),
            ],
        ];
    }
}
