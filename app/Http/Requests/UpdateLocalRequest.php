<?php

namespace App\Http\Requests;

use App\Models\Content;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class UpdateLocalRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        $type = $this->route('type');

        $basicRules = [
            'ref_id' => 'nullable|integer',
            'parent_id' => 'nullable|exists:contents,id',
            'name' => 'required|string|max:191',
            'private' => 'nullable|boolean',
            'visible' => 'nullable|boolean',
            'order' => 'nullable|integer',
        ];

        $typeRules = config("local.{$type}.fields", []);

        $rules = array_merge($typeRules, $basicRules);

        foreach ($rules as $field => $rule) {
            if (stripos($rule, 'sometimes') === false) {
                $rules[$field] = 'sometimes|' . $rule;
            }
        }

        return $rules;
    }

    public function validationData()
    {
        $data = parent::validationData();

        $parentId = Arr::get($data, 'parent_id', null);

        if (!empty($parentId)) {
            $content = Content::where('uuid', $parentId)
                ->where('tenant_id', tenant('id'))
                ->first();

            $data['parent_id'] = optional($content)->id;
        }

        return $data;
    }
}
