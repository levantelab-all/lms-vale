<?php

namespace App\Http\Requests;

use App\Models\Team;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class ProfileRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        $id = auth()->user()->id;

        return [
            'name' => 'sometimes|required|string|max:191',
            'email' => [
                'sometimes',
                'required',
                'string',
                'max:191',
                'email',
                Rule::unique('users')->ignore('id')->where(function ($query) {
                    return $query->where('tenant_id', tenant('id'));
                }),
            ],
            'phone' => 'sometimes|nullable|string|max:191',
            'cpf' => [
                'sometimes',
                'required',
                'string',
                'max:191',
                'cpf',
                Rule::unique('users')->ignore('id')->where(function ($query) {
                    return $query->where('tenant_id', tenant('id'));
                }),
            ],
            'password' => [
                'sometimes',
                'required',
                'confirmed',
                Password::min(8)->uncompromised(),
            ],
            'teams' => 'sometimes|array',
            'teams.*' => 'nullable|exists:teams,id',
        ];
    }

    public function validationData()
    {
        $data = parent::validationData();

        if (Arr::exists($data, 'teams')) {
            $teamsId = Arr::get($data, 'teams', []);

            if (!empty($teamsId)) {
                $teamsId = Team::whereIn('uuid', $teamsId)
                    ->where('tenant_id', tenant('id'))
                    ->pluck('id')
                    ->toArray();

                $data['teams'] = $teamsId;
            }
        }

        return $data;
    }
}
