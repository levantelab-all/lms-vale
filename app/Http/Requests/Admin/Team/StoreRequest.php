<?php

namespace App\Http\Requests\Admin\Team;

use App\Models\Content;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return is_admin();
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:191',
            'ref_id' => 'nullable|exists:contents,id',
            'users' => 'sometimes|array',
            'users.*' => 'nullable|exists:users,id',
        ];
    }

    public function validationData()
    {
        $data = parent::validationData();

        $refId = Arr::get($data, 'ref_id', null);

        if (!empty($refId)) {
            $content = Content::where('uuid', $refId)
                ->where('tenant_id', tenant('id'))
                ->first();

            $data['ref_id'] = optional($content)->id;
        }

        if (Arr::exists($data, 'users')) {
            $userIds = Arr::get($data, 'users', []);

            if (!empty($userIds)) {
                $userIds = User::whereIn('uuid', $userIds)
                    ->where('tenant_id', tenant('id'))
                    ->pluck('id')
                    ->toArray();

                $data['users'] = $userIds;
            }
        }

        return $data;
    }
}
