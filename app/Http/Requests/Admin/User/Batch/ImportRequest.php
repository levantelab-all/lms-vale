<?php

namespace App\Http\Requests\Admin\User\Batch;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ImportRequest extends FormRequest
{
    public function authorize()
    {
        return is_admin();
    }

    public function rules()
    {
        return [
            'users' => 'required|array',

            'users.*.name' => 'required|string|max:191',
            'users.*.cpf' => [
                'required',
                'string',
                'max:191',
                'cpf',
                Rule::unique('users', 'cpf')->where(function ($query) {
                    return $query->where('tenant_id', tenant('id'));
                })
            ],
            'users.*.phone' => 'nullable|string|max:191',
            'users.*.email' => [
                'required',
                'string',
                'max:191',
                'email',
                Rule::unique('users', 'email')->where(function ($query) {
                    return $query->where('tenant_id', tenant('id'));
                })
            ],
        ];
    }
}
