<?php

namespace App\Http\Requests\Admin\User\Batch;

use Illuminate\Foundation\Http\FormRequest;

class ValidateRequest extends FormRequest
{
    public function authorize()
    {
        return is_admin();
    }

    public function rules()
    {
        return [
            'file' => 'required|file|mimes:xlsx',
        ];
    }
}
