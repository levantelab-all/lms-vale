<?php

namespace App\Http\Requests\Admin\User;

use App\Models\Team;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return is_admin();
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:191',
            'email' => [
                'required',
                'string',
                'max:191',
                'email',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('tenant_id', tenant('id'));
                }),
            ],
            'phone' => 'nullable|string|max:191',
            'cpf' => [
                'required',
                'string',
                'max:191',
                'cpf',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('tenant_id', tenant('id'));
                }),
            ],
            'level' => 'required|in:admin,user',
            'teams' => 'sometimes|array',
            'teams.*' => 'nullable|exists:teams,id',
        ];
    }

    public function validationData()
    {
        $data = parent::validationData();

        if (Arr::exists($data, 'email')) {
            $data['email'] = trim(Arr::get($data, 'email'));
        }

        if (Arr::exists($data, 'cpf')) {
            $data['cpf'] = clean_cpf(Arr::get($data, 'cpf'));
        }

        if (Arr::exists($data, 'phone')) {
            $data['phone'] = clean_phone(Arr::get($data, 'phone'));
        }

        if (Arr::exists($data, 'teams')) {
            $teamsId = Arr::get($data, 'teams', []);

            if (!empty($teamsId)) {
                $teamsId = Team::whereIn('uuid', $teamsId)
                    ->where('tenant_id', tenant('id'))
                    ->pluck('id')
                    ->toArray();

                $data['teams'] = $teamsId;
            }
        }

        return $data;
    }
}
