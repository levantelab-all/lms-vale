<?php

namespace App\Http\Requests\Project\Post;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'title' => 'required|string',
            'body' => 'nullable|string',
            'type' => 'required|string|max:191',
            'images' => 'nullable|array',
            'images.*' => 'required|image',
        ];
    }
}
