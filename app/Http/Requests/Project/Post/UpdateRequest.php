<?php

namespace App\Http\Requests\Project\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'title' => 'sometimes|required|string',
            'body' => 'sometimes|nullable|string',
            'type' => 'sometimes|required|string|max:191',
            'photos' => 'sometimes|nullable|array',
            'images' => 'sometimes|nullable|array',
            'images.*' => 'required|image',
        ];
    }
}
