<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'title' => 'sometimes|nullable|string|max:191',
            'description' => 'sometimes|nullable|string',
            'cover_url' => 'sometimes|nullable|url',
            'visible' => 'sometimes|boolean',
        ];
    }
}
