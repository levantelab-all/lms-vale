<?php

namespace App\Http\Requests\Project\Certificate;

use App\Models\Content;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'template' => 'required|string',
            'parameters' => 'nullable|array',
        ];
    }
}
