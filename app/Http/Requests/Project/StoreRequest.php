<?php

namespace App\Http\Requests\Project;

use App\Models\Team;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return is_admin();
    }

    public function rules()
    {
        return [
            'team_id' => 'required|exists:teams,id',
        ];
    }

    public function validationData()
    {
        $data = parent::validationData();

        $teamId = Arr::get($data, 'team_id', null);

        if (!empty($teamId)) {
            $team = Team::where('uuid', $teamId)
                ->where('tenant_id', tenant('id'))
                ->first();

            $data['team_id'] = optional($team)->id;
        }

        return $data;
    }
}
