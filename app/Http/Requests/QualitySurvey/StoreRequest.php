<?php

namespace App\Http\Requests\QualitySurvey;

use App\Models\Content;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'ref_id' => 'required|integer|min:1',
            'question1' => 'required|string',
            'question2' => 'required|string',
            'question3' => 'required|string',
            'question4' => 'required|string',
            'question5' => 'required|string',
            'question6' => 'required|string',
            'question7' => 'required|string',
            'question8' => 'required|string',
            'question9' => 'required|string',
            'question10' => 'required|string',
            'question11' => 'required|string',
            'question12' => 'required|string',
        ];
    }

    public function validationData()
    {
        $data = parent::validationData();

        $uuid = Arr::get($data, 'ref_id', null);

        if (!empty($uuid)) {
            $content = Content::where('uuid', $uuid)
                ->where('tenant_id', tenant('id'))
                ->first();

            $data['ref_id'] = optional($content)->id;
        }

        return $data;
    }
}
