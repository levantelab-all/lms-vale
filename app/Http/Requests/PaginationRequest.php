<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaginationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'page' => 'sometimes|nullable|integer|min:1',
            'size' => 'sometimes|nullable|integer|min:0',
        ];
    }
}
