<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnalyticsRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'type' => 'required|string|max:255',
            'label1' => 'required|string|max:255',
            'label2' => 'nullable|string|max:255',
            'label3' => 'nullable|string|max:255',
            'progress' => 'nullable|integer|min:0|max:100',
            'city' => 'nullable|string|max:255',
            'ubs' => 'nullable|string|max:255',
        ];
    }
}
