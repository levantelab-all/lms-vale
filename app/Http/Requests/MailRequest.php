<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailRequest extends FormRequest
{
    public function authorize()
    {
        return is_user();
    }

    public function rules()
    {
        return [
            'to.*' => 'required|email',
            'cc.*' => 'nullable|email',
            'bcc.*' => 'nullable|email',
            'subject' => 'required|string',
            'body' => 'required|string',
            'text' => 'nullable|string',
        ];
    }
}
