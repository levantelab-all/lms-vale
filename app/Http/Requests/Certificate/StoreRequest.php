<?php

namespace App\Http\Requests\Certificate;

use App\Models\Content;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'ref_id' => 'required|exists:contents,id',
            'template' => 'required|string',
            'parameters' => 'required|array',
        ];
    }

    public function validationData()
    {
        $data = parent::validationData();

        $refId = Arr::get($data, 'ref_id', null);

        if (!empty($refId)) {
            $content = Content::where('uuid', $refId)
                ->where('tenant_id', tenant('id'))
                ->first();

            $data['ref_id'] = optional($content)->id;
        }

        return $data;
    }
}
