<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [];

        $data['id'] = $this->uuid;
        $data['name'] = $this->name;
        if ($this->relationLoaded('content')) {
            if (!is_null($this->content)) {
                $data['ref_id'] = $this->content->uuid;
                $data['ref'] = new ContentResource($this->content);
            } else {
                $data['ref_id'] = null;
            }
        } else {
            $data['ref_id'] = null;
        }
        $data['properties'] = $this->properties;
        if ($this->relationLoaded('users')) {
            $data['users'] = UserResource::collection($this->users);
        }
        if (isset($this->users_count)) {
            $data['users_count'] = (int) $this->users_count;
        }
        if ($this->relationLoaded('workshop')) {
            $data['workshop'] = (!empty($this->workshop))
                ? new ContentResource($this->workshop)
                : null;
        }
        if ($this->relationLoaded('projects')) {
            $data['projects'] = ProjectResource::collection($this->projects);
        }
        if (isset($this->projects_count)) {
            $data['projects_count'] = (int) $this->projects_count;
        }
        $data['created_at'] = $this->created_at->toIso8601String();
        $data['updated_at'] = $this->updated_at->toIso8601String();

        return $data;
    }
}
