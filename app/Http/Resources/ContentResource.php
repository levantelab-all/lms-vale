<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class ContentResource extends JsonResource
{
    public function toArray($request)
    {
        $scope = (is_null($this->tenant_id))
            ? 'global'
            : 'local';
        $config = config("{$scope}.{$this->type}");

        $data = [];

        $data['id'] = $this->uuid;
        $data['type'] = $this->type;
        $data['global'] = (is_null($this->tenant_id))
            ? true
            : false;
        $data['ref_id'] = $this->ref_id;
        $data['name'] = $this->name;
        $data['private'] = $this->private;
        $data['visible'] = $this->visible;
        $data['order'] = $this->order;
        $data['properties'] = $this->properties;
        foreach (Arr::get($config, 'includes', []) as $include => $embedType) {
            if ($this->relationLoaded($include)) {
                if ($include === 'owner') {
                    $data['owner'] = new SimpleUserResource($this->owner);
                } else if ($include === 'teams') {
                    $data['teams'] = TeamResource::collection($this->teams);
                } else {
                    if ($embedType === 'many') {
                        $data[$include] = ContentResource::collection($this->$include);
                    } else if ($embedType === 'single') {
                        $data[$include] = new ContentResource($this->$include);
                    }
                }
            }

            $fieldCount = "{$include}_count";
            if (isset($this->$fieldCount)) {
                $data[$fieldCount] = (int) $this->$fieldCount;
            }
        }
        $data['created_at'] = $this->created_at->toIso8601String();
        $data['updated_at'] = $this->updated_at->toIso8601String();

        return $data;
    }
}
