<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CertificateResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [];

        $data['id'] = $this->uuid;
        $data['template'] = $this->template;
        $data['url'] = $this->url;
        $data['parameters'] = $this->parameters;
        $data['created_at'] = $this->created_at->toIso8601String();
        $data['updated_at'] = $this->updated_at->toIso8601String();

        return $data;
    }
}
