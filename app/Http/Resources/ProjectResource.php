<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [];

        $data['id'] = $this->uuid;
        if ($this->relationLoaded('team')) {
            $data['team'] = new TeamResource($this->team);
        }
        $data['title'] = $this->title;
        $data['description'] = $this->description;
        $data['cover_url'] = $this->cover_url;
        $data['visible'] = (bool) $this->visible;
        $data['properties'] = $this->properties;
        if (!empty($this->certificate_url)) {
            $data['certificate'] = [
                'url' => $this->certificate_url,
                'created_at' => $this->certificate_created_at->toIso8601String(),
            ];
        } else {
            $data['certificate'] = null;
        }
        if ($this->relationLoaded('posts')) {
            $data['posts'] = PostResource::collection($this->posts);
        }
        if (isset($this->posts_count)) {
            $data['posts_count'] = (int) $this->posts_count;
        }
        $data['created_at'] = $this->created_at->toIso8601String();
        $data['updated_at'] = $this->updated_at->toIso8601String();

        return $data;
    }
}
