<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserWithTokenResource extends JsonResource
{
    protected $token;
    protected $expiresIn;

    public function __construct($resource, $token, $expiresIn)
    {
        parent::__construct($resource);

        $this->token = $token;
        $this->expiresIn = $expiresIn;
    }

    public function toArray($request)
    {
        $data = [];

        $data['id'] = $this->uuid;
        $data['name'] = $this->name;
        $data['email'] = $this->email;
        $data['phone'] = $this->phone;
        $data['cpf'] = $this->cpf;
        $data['level'] = $this->level;
        $data['properties'] = $this->properties;
        if ($this->relationLoaded('teams')) {
            $data['teams'] = TeamResource::collection($this->teams);
        }
        if (isset($this->teams_count)) {
            $data['teams_count'] = (int) $this->teams_count;
        }
        if ($this->relationLoaded('certificates')) {
            $data['certificates'] = CertificateResource::collection($this->certificates);
        }
        if (isset($this->certificates_count)) {
            $data['certificates_count'] = (int) $this->certificates_count;
        }

        $data['token'] = [
            'access_token' => $this->token,
            'token_type' => 'bearer',
            'expires_in' => $this->expiresIn,
        ];
        $data['created_at'] = $this->created_at->toIso8601String();
        $data['updated_at'] = $this->updated_at->toIso8601String();

        return $data;
    }
}
