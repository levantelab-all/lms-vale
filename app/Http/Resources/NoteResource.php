<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NoteResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [];

        $data['id'] = $this->uuid;
        $data['title'] = $this->title;
        $data['body'] = $this->body;
        $data['properties'] = $this->properties;
        $data['created_at'] = $this->created_at->toIso8601String();
        $data['updated_at'] = $this->updated_at->toIso8601String();

        return $data;
    }
}
