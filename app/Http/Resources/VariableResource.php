<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VariableResource extends JsonResource
{
    public function toArray($request)
    {
        $value = json_decode($this->value);
        if ($value === null) {
            $value = $this->value;
        }

        return [
            'group' => $this->group,
            'key' => $this->key,
            'value' => $value,
            'created_at' => $this->created_at->toIso8601String(),
            'updated_at' => $this->updated_at->toIso8601String(),
        ];
    }
}
