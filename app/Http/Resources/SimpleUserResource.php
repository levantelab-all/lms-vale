<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class SimpleUserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->uuid,
            'name' => $this->name,
            'level' => $this->level,
            'profile_img' => Arr::get($this->properties, 'profile_img'),
        ];
    }
}
