<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UploadResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->uuid,
            'owner' => (optional(auth()->user())->id == $this->owner_id)
                ? true
                : false,
            'name' => $this->name,
            'url' => $this->url,
            'created_at' => $this->created_at->toIso8601String(),
            'updated_at' => $this->updated_at->toIso8601String(),
        ];
    }
}
