<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [];

        $data['id'] = $this->uuid;
        $data['title'] = $this->title;
        $data['body'] = $this->body;
        $data['type'] = $this->type;
        $data['photos'] = $this->photos;
        if ($this->relationLoaded('user')) {
            $data['owner'] = new SimpleUserResource($this->user);
        }
        $data['created_at'] = $this->created_at->toIso8601String();
        $data['updated_at'] = $this->updated_at->toIso8601String();

        return $data;
    }
}
