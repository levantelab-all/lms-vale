<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerSQLDebug();
    }

    public function boot()
    {
        //
    }

    private function registerSQLDebug()
    {
        \DB::listen(function ($event) {
            if (config('app.debug_sql', false)) {
                $headline = "[{$event->time}ms]";
                $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                foreach ($traces as $trace) {
                    if (isset($trace['file']) && ! str_contains($trace['file'], DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR)) {
                        $file = $trace['file'];
                        $file = str_replace(base_path() . DIRECTORY_SEPARATOR, '', $file);

                        $headline = $headline . " [{$trace['line']}] {$file}";
                        break;
                    }
                }
                $files = [];
                foreach ($traces as $trace) {
                    if (isset($trace['file']) && ! str_contains($trace['file'], DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR)) {
                        $file = $trace['file'];
                        $file = str_replace(base_path() . DIRECTORY_SEPARATOR, '', $file);

                        $files[] = "[{$trace['line']}] {$file}";
                    }
                }

                foreach ($event->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $event->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } elseif (is_string($binding)) {
                        $event->bindings[$i] = "'$binding'";
                    }
                }

                $sql = str_replace(array('%', '?'), array('%%', '%s'), $event->sql);
                $sql = vsprintf($sql, $event->bindings);

                \Log::debug("{$headline}\n\t{$sql}\n\t" . implode("\n\t", $files) . "\n");
            }
        });
    }
}
