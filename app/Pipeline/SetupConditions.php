<?php

namespace App\Pipeline;

use Closure;
use Illuminate\Support\Arr;
use Spatie\QueryBuilder\AllowedFilter;

class SetupConditions
{
    public function handle($payload, Closure $next)
    {
        $config = $payload['config'];
        $queryBuilder = $payload['queryBuilder'];

        if (isset($config['conditions']) && is_array($config['conditions'])) {
            $conditions = $config['conditions'];

            foreach ($conditions as $condition) {
                switch ($condition['oper']) {
                    case 'null':
                        $queryBuilder->whereNull($condition['field']);
                        break;
                    case 'not_null':
                        $queryBuilder->whereNotNull($condition['field']);
                        break;
                    default:
                        if (isset($condition['oper'])) {
                            $queryBuilder->where($condition['field'], $condition['oper'], $condition['value']);
                        } else {
                            $queryBuilder->where($condition['field'], $condition['value']);
                        }
                        break;
                }
            }
        }

        $canBeOrphan = Arr::get($config, 'can-be-orphan', true);
        if (!$canBeOrphan) {
            $queryBuilder->whereNotNull('parent_id');
        }

        return $next(compact('config', 'queryBuilder'));
    }
}
