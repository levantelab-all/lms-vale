<?php

namespace App\Pipeline;

use App\Http\Resources\ContentResource;
use Closure;

class ReturnResources
{
    public function handle($payload, Closure $next)
    {
        $config = $payload['config'];
        $queryBuilder = $payload['queryBuilder'];

        if (isset($config['page-size']) && $config['page-size'] > 0) {
            $pageSize = (int) request('size', $config['page-size']);

            if ($pageSize > 0) {
                return $next(ContentResource::collection($queryBuilder->paginate($pageSize)));
            }
        }

        $rows = $queryBuilder->get();

        return $next(ContentResource::collection($rows)
            ->additional([
                'meta' => [
                    'total' => $rows->count(),
                ],
            ]));
    }
}
