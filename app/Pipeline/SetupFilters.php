<?php

namespace App\Pipeline;

use Closure;
use Spatie\QueryBuilder\AllowedFilter;

class SetupFilters
{
    public function handle($payload, Closure $next)
    {
        $config = $payload['config'];
        $queryBuilder = $payload['queryBuilder'];

        $filters = [
            AllowedFilter::partial('name'),
            AllowedFilter::exact('id', 'uuid'),
            AllowedFilter::exact('ref_id'),
            AllowedFilter::exact('private'),
            AllowedFilter::exact('visible'),
        ];

        if (isset($config['filters']) && is_array($config['filters'])) {
            $filters = array_merge($filters, $config['filters']);
        }

        $queryBuilder->allowedFilters($filters);

        return $next(compact('config', 'queryBuilder'));
    }
}
