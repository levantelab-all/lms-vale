<?php

namespace App\Pipeline;

use App\Http\Resources\ContentResource;
use Closure;

class ReturnResource
{
    public function handle($payload, Closure $next)
    {
        $config = $payload['config'];
        $queryBuilder = $payload['queryBuilder'];

        $content = $queryBuilder->first();

        abort_if(is_null($content), 404, 'Not found');

        return $next(new ContentResource($content));
    }
}
