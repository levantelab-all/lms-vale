<?php

namespace App\Pipeline;

use Closure;

class SetupIncludes
{
    public function handle($payload, Closure $next)
    {
        $config = $payload['config'];
        $queryBuilder = $payload['queryBuilder'];

        if (isset($config['includes']) && is_array($config['includes'])) {
            $queryBuilder->allowedIncludes(array_keys($config['includes']));
        }

        return $next(compact('config', 'queryBuilder'));
    }
}
