<?php

namespace App\Pipeline;

use Closure;
use Spatie\QueryBuilder\AllowedSort;

class SetupOrders
{
    public function handle($payload, Closure $next)
    {
        $config = $payload['config'];
        $queryBuilder = $payload['queryBuilder'];

        $orders = [
            AllowedSort::field('name'),
            AllowedSort::field('order'),
            AllowedSort::field('created_at'),
            AllowedSort::field('updated_at'),
        ];

        if (isset($config['orders']) && is_array($config['orders'])) {
            $orders = array_merge($orders, $config['orders']);
        }

        $queryBuilder->allowedSorts($orders);

        if (isset($config['order']) && !empty($config['order'])) {
            $queryBuilder->defaultSort($config['order']);
        } else {
            $queryBuilder->defaultSort('order');
        }

        return $next(compact('config', 'queryBuilder'));
    }
}
