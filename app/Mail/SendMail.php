<?php

namespace App\Mail;

use App\Http\Requests\MailRequest;
use App\Models\Tenant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $tenant;

    public $subject;
    public $body;
    public $text;

    public function __construct(Tenant $tenant, $subject, $body, $text)
    {
        $this->tenant = $tenant;

        $this->subject = $subject;
        $this->body = $body;
        $this->text = $text;
    }

    public function build()
    {
        $this->subject($this->subject);

        if (!empty($this->text)) {
            $this->text($this->text);
        }

        // $tenant = $this->tenant;
        // $this->withSwiftMessage(function ($message) use ($tenant) {
        //     $message->getHeaders()->addTextHeader(
        //         'X-Tenant', $tenant->uuid
        //     );
        // });

        return $this->view('mail');
    }
}
