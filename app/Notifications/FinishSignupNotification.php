<?php

namespace App\Notifications;

use App\Models\Tenant;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FinishSignupNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $tenant, $password;

    public function __construct(Tenant $tenant, $password)
    {
        $this->tenant = $tenant;
        $this->password = $password;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $view = "mail.finish-signup-notification.{$this->tenant->uuid}";

        return (new MailMessage)
            ->subject('Finalize seu cadastro')
            ->view($view, [
                'tenant' => $this->tenant,
                'user' => $notifiable,
                'password' => $this->password,
            ]);
    }
}
