# Global types

## 0. common

**properties:**

- id - UUID
- ref_id - ID de referência externa (padrão NULL)
- parent_id - ID do conteúdo PAI (padrão NULL)
- owner_id - ID do proprietário do conteúdo
- name - nome do conteúdo
- properties - propriedades JSON (padrão NULL)
- private - 0
- visible - 1
- order - campo para ordenação (padrão 0)

**fields:**

- ref_id - nullable|integer
- parent_id - nullable|exists:contents,id
- name - required|string|max:191
- private - nullable|boolean
- visible - nullable|boolean
- order - nullable|integer

**filters:**

- name - partial
- id - exact
- ref_id - exact
- private - exact
- visible - exact

**sorts:**

- name
- order (padrão)
- created_at
- updated_at

**read-permissions:**

- guest
- user
- admin

**can-be-orphan:** true



## 1. states

**ref_id: ** código do IBGE

**parent_id:** NULL

**relations:**

- cities - hasMany

**includes:**

- cities - collection

**page-size:** 0



## 2. cities

**ref_id:** código do IBGE com 6 dígitos

**parent_id**: UUID do estado

**properties:**

- id7 - código do IBGE com 7 dígitos
- state_id - código IBGE do estado

**relations:**

- state - belongsTo

**includes:**

- state - model

**filters:**

- state_id - exact

**orders:**

- state_id

**page-size:** 25

**can-be-orphan:** false


