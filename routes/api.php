<?php

Route::get('/', [App\Http\Controllers\VersionController::class, 'index']);

Route::group(['prefix' => '/global/{type}', 'middleware' => ['types:global']], function () {
    Route::get('/', App\Http\Controllers\Global\IndexController::class);
    Route::get('/{uuid}', App\Http\Controllers\Global\ShowController::class);
});

Route::group(['middleware' => ['tenant']], function () {
    Route::group(['prefix' => '/local/{type}', 'middleware' => ['types:local']], function () {
        Route::get('/', App\Http\Controllers\Local\IndexController::class);
        Route::post('/', App\Http\Controllers\Local\StoreController::class)
            ->middleware('auth');
        Route::get('/{uuid}', App\Http\Controllers\Local\ShowController::class);
        Route::post('/{uuid}', App\Http\Controllers\Local\UpdateController::class)
            ->middleware('auth');
        Route::delete('/{uuid}', App\Http\Controllers\Local\DeleteController::class)
            ->middleware('auth');
    });

    Route::group(['prefix' => '/upload'], function () {
        Route::post('/', App\Http\Controllers\Upload\StoreController::class)
            ->middleware('auth');
        Route::get('/{upload:uuid}', App\Http\Controllers\Upload\ShowController::class);
        Route::delete('/{upload:uuid}', App\Http\Controllers\Upload\DeleteController::class)
            ->middleware('auth');
    });

    Route::group(['prefix' => '/variables/{group}', 'middleware' => ['auth']], function () {
        Route::get('/', App\Http\Controllers\Variable\Batch\IndexController::class);
        Route::post('/', App\Http\Controllers\Variable\Batch\StoreController::class);
        Route::delete('/', App\Http\Controllers\Variable\Batch\DeleteController::class);

        Route::get('/{key}', App\Http\Controllers\Variable\ShowController::class);
        Route::post('/{key}', App\Http\Controllers\Variable\StoreController::class);
        Route::delete('/{key}', App\Http\Controllers\Variable\DeleteController::class);
    });

    Route::group(['prefix' => '/auth'], function () {
        Route::post('/login', App\Http\Controllers\Auth\LoginController::class);
        Route::post('/signup', App\Http\Controllers\Auth\SignupController::class);
        Route::post('/forgot-password', App\Http\Controllers\Auth\PasswordResetLinkController::class);
        Route::post('/reset-password', App\Http\Controllers\Auth\NewPasswordController::class);
        Route::post('/refresh', App\Http\Controllers\Auth\RefreshController::class)
            ->middleware('auth');
        Route::post('/logout', App\Http\Controllers\Auth\LogoutController::class)
            ->middleware('auth');
    });

    Route::group(['prefix' => '/profile', 'middleware' => ['auth']], function () {
        Route::get('/', App\Http\Controllers\Profile\ShowController::class);
        Route::post('/', App\Http\Controllers\Profile\StoreController::class);
    });

    Route::group(['prefix' => '/teams', 'middleware' => ['auth']], function () {
        Route::get('/', App\Http\Controllers\Team\IndexController::class);
        Route::get('/{uuid}', App\Http\Controllers\Team\ShowController::class);
    });

    Route::group(['prefix' => '/projects', 'middleware' => ['auth']], function () {
        Route::get('/', App\Http\Controllers\Project\IndexController::class);
        Route::post('/', App\Http\Controllers\Project\StoreController::class)
            ->middleware(['is_admin']);
        Route::get('/{uuid}', App\Http\Controllers\Project\ShowController::class);
        Route::post('/{uuid}', App\Http\Controllers\Project\UpdateController::class);
        Route::delete('/{uuid}', App\Http\Controllers\Project\DeleteController::class)
            ->middleware(['is_admin']);

        Route::group(['prefix' => '/{projectUuid}/posts'], function () {
            Route::get('/', App\Http\Controllers\Project\Post\IndexController::class);
            Route::post('/', App\Http\Controllers\Project\Post\StoreController::class);
            Route::get('/{postUuid}', App\Http\Controllers\Project\Post\ShowController::class);
            Route::post('/{postUuid}', App\Http\Controllers\Project\Post\UpdateController::class);
            Route::delete('/{postUuid}', App\Http\Controllers\Project\Post\DeleteController::class);
        });

        Route::post('/{projectUuid}/certificate', App\Http\Controllers\Project\Certificate\StoreController::class)
            ->middleware(['is_admin']);
    });

    Route::group(['prefix' => '/notes', 'middleware' => ['auth']], function () {
        Route::get('/', App\Http\Controllers\Note\IndexController::class);
        Route::post('/', App\Http\Controllers\Note\StoreController::class);
        Route::get('/{uuid}', App\Http\Controllers\Note\ShowController::class);
        Route::post('/{uuid}', App\Http\Controllers\Note\UpdateController::class);
        Route::delete('/{uuid}', App\Http\Controllers\Note\DeleteController::class);
    });

    Route::group(['prefix' => '/quality-survey', 'middleware' => ['auth']], function () {
        Route::post('/', App\Http\Controllers\QualitySurvey\StoreController::class);
    });

    Route::group(['prefix' => '/certificates'], function () {
        Route::post('/', App\Http\Controllers\Certificate\StoreController::class)
            ->middleware(['auth']);
        Route::get('/{uuid}', App\Http\Controllers\Certificate\ShowController::class);
    });

    Route::post('/analytics', App\Http\Controllers\AnalyticsController::class)
        ->middleware(['auth']);

    Route::post('/mail', App\Http\Controllers\MailController::class)
        ->middleware(['auth']);

    Route::get('/mail/admin-users', App\Http\Controllers\AdminUsersController::class)
        ->middleware(['auth']);

    Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'is_admin']], function () {
        Route::group(['prefix' => '/users'], function () {
            Route::get('/', App\Http\Controllers\Admin\User\IndexController::class);
            Route::post('/', App\Http\Controllers\Admin\User\StoreController::class);
            Route::get('/{uuid}', App\Http\Controllers\Admin\User\ShowController::class);
            Route::post('/{uuid}', App\Http\Controllers\Admin\User\UpdateController::class);
            Route::delete('/{uuid}', App\Http\Controllers\Admin\User\DeleteController::class);

            Route::post('/batch/validate', App\Http\Controllers\Admin\User\Batch\ValidateController::class);
            Route::post('/batch/import', App\Http\Controllers\Admin\User\Batch\ImportController::class);
        });

        Route::group(['prefix' => '/teams'], function () {
            Route::get('/', App\Http\Controllers\Admin\Team\IndexController::class);
            Route::post('/', App\Http\Controllers\Admin\Team\StoreController::class);
            Route::get('/{uuid}', App\Http\Controllers\Admin\Team\ShowController::class);
            Route::post('/{uuid}', App\Http\Controllers\Admin\Team\UpdateController::class);
            Route::delete('/{uuid}', App\Http\Controllers\Admin\Team\DeleteController::class);
        });
    });
});
