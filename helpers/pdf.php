<?php

function pdf()
{
    $mpdf = new \Mpdf\Mpdf([
        'mode' => config('pdf.mode'),
        'format' => config('pdf.format'),
        'default_font_size' => config('pdf.defaultFontSize'),
        'default_font' => config('pdf.defaultFont'),
        'margin_left' => config('pdf.marginLeft'),
        'margin_right' => config('pdf.marginRight'),
        'margin_top' => config('pdf.marginTop'),
        'margin_bottom' => config('pdf.marginBottom'),
        'margin_header' => config('pdf.marginHeader'),
        'margin_footer' => config('pdf.marginFooter'),
        'orientation' => config('pdf.orientation'),
        // 'tempDir' => sys_get_temp_dir(),
        'tempDir' => storage_path('app/pdf-cache'),
    ]);

    $permissions = [];
    foreach (config('pdf.protection.permissions') as $perm => $enable) {
        if ($enable) {
            $permissions[] = $perm;
        }
    }
    $mpdf->SetProtection(
        $permissions,
        config('pdf.protection.user_password'),
        config('pdf.protection.owner_password'),
        config('pdf.protection.length')
    );
    $mpdf->SetAuthor(config('pdf.author'));
    $mpdf->SetWatermarkText(config('pdf.watermark'));
    $mpdf->showWatermarkText = config('pdf.showWatermark');
    $mpdf->watermark_font = config('pdf.watermarkFont');
    $mpdf->watermarkTextAlpha = config('pdf.watermarkTextAlpha');
    $mpdf->SetDisplayMode(config('pdf.displayMode'));

    return $mpdf;
}
