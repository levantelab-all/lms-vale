<?php

use App\Models\User;

function is_admin(?User $user = null)
{
    $user = (is_null($user)) ? auth()->user() : $user;

    if (is_null($user)) {
        return false;
    }

    return in_array($user->level, [
        User::LEVEL_ADMIN,
    ]);
}

function is_user(?User $user = null)
{
    $user = (is_null($user)) ? auth()->user() : $user;

    if (is_null($user)) {
        return false;
    }

    return in_array($user->level, [
        User::LEVEL_ADMIN,
        User::LEVEL_USER,
    ]);
}

function authorize_resource($scope, $resource, $action, $defaultPermissions)
{
    $user = auth()->user();
    $userLevel = (!is_null($user))
        ? $user->level
        : 'guest';

    $permissions = config("{$scope}.{$resource}.{$action}-permissions", $defaultPermissions);

    if (!in_array($userLevel, $permissions)) {
        abort(403, 'Unauthorized');
    }
}

function authorize_read_resource($scope, $resource)
{
    return authorize_resource($scope, $resource, 'read', [
        'guest',
        \App\Models\User::LEVEL_USER,
        \App\Models\User::LEVEL_ADMIN,
    ]);
}

function authorize_write_resource($scope, $resource)
{
    return authorize_resource($scope, $resource, 'write', [
        \App\Models\User::LEVEL_USER,
        \App\Models\User::LEVEL_ADMIN,
    ]);
}
