<?php

function content_setup_relationships($scope, $type)
{
    $config = config("{$scope}.{$type}");

    if (isset($config['relations']) && is_array($config['relations'])) {
        \App\Models\Content::bootRelations($config['relations'], $scope);
    }
}

function find_project_query($uuid)
{
    $user = auth()->user();

    $query = App\Models\Project::where('uuid', $uuid)
        ->where('tenant_id', tenant('id'));

    if (!is_admin($user)) {
        $user->load('teams');

        $teamIds = $user->teams->pluck('id');

        $query = $query->where(function ($q) use ($teamIds) {
            $q->where('visible', true)
                ->orWhereIn('team_id', $teamIds);
        });
    }

    return $query;
}

function find_project($uuid)
{
    $query = find_project_query($uuid);

    return $query->first();
}
