<?php

use Illuminate\Support\Facades\Validator;

function validate($fields, $rules)
{
    if (!is_array($fields)) {
        $fields = ['default' => $fields];
    }

    if (!is_array($rules)) {
        $rules = ['default' => $rules];
    }

    return Validator::make($fields, $rules)->passes();
}

function clean_value($val)
{
    return str_replace(['.', '/', '-', ' ', '\\', '(', ')', ','], ['', '', '', '', '', '', '', ''], $val);
}

function clean_cpf($val)
{
    return clean_value($val);
}

function clean_phone($val)
{
    return clean_value($val);
}

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;

    for ($i = 0; $i<=strlen($mask)-1; $i++) {
        if ($mask[$i] == '#') {
            if (isset($val[$k])) {
                $maskared .= $val[$k++];
            }
        } else {
            if (isset($mask[$i])) {
                $maskared .= $mask[$i];
            }
        }
    }

    return $maskared;
}

function mask_cpf($val)
{
    return (!empty($val))
        ? mask($val, '###.###.###-##')
        : '';
}
