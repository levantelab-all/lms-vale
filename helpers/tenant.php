<?php

function tenant($field = null)
{
    try {
        $tenant = app('tenant');
    } catch (\Exception $e) {
        $tenant = null;
    }

    return (!is_null($field))
        ? optional($tenant)->$field
        : $tenant;
}
