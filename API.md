# 1. Definições

## 1.1. Tenants

1. **94632844-067a-466c-ad06-3c14db9e1c58** - CEDAPS Virtual
2. **11255f7c-1dc3-4b86-9900-7681c2167090** - Ciclo Virtual

## 1.2 Usuário padrão (desenvolvimento)

- Email: admin@levantelab.com.br
- CPF: 21109100035
- Senha: 1a2a3a

## 1.3. HTTP Headers

1. X-Tenant: <uuid> - fornecer o ID do tenant desejado
2. Authorization: Beader <access_token> - fornecer o token de autenticação obtido após o login (ou refresh)

## 1.4. Outras documentações

1. **Laravel Query Builder:** https://spatie.be/docs/laravel-query-builder/v3/introduction


# 2. APIs

## 2.1. Autenticação

### 2.1.1. Login
**POST /auth/login**

Autenticação: não

Payload:

- username - required 
  - pode ser o email ou CPF
- password - required

Resposta:
- 200: usuário + token
- 401: não autenticado
- 417: tenant não encontrado
- 422: validação falhou

### 2.1.2. Signup

**POST /auth/signup**

Autenticação: não

Payload:

- name - required|string|max:191
- email - required|string|max:191|email|unique:users
- phone - nullable|string|max:191
- cpf - required|string|max:191|cpf|unique:users
- password - required|confirmed|Password::min(8)->uncompromised()
- teams - sometimes|array
- teams.* - nullable|exists:teams,id
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 201: usuário + token
- 417: tenant não encontrado
- 422: validação falhou

### 2.1.3. Token refresh
**POST /auth/refresh**

Autenticação: sim

Resposta:

- 200: usuário + token
- 401: não autenticado
- 417: tenant não encontrado

### 2.1.4. Logout
**POST /auth/logout**

Autenticação: sim

Resposta:

- 204: ok (sem conteúdo)
- 401: não autenticado
- 417: tenant não encontrado

### 2.1.5. Profile

**GET /profile**

Autenticação: sim

Resposta:

- 200: usuário
- 401: não autenticado
- 417: tenant não encontrado

### 2.1.6. Atualizar profile

**POST /profile**

Autenticação: sim

Payload:

- name - sometimes|required|string|max:191
- email - sometimes|required|string|max:191|email|unique:users
- phone - sometimes|nullable|string|max:191
- cpf - sometimes|required|string|max:191|cpf|unique:users
- password - sometimes|required|confirmed|Password::min(8)->uncompromised()
- teams - sometimes|array
- teams.* - nullable|exists:teams,id
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 200: usuário
- 401: não autenticado
- 417: tenant não encontrado
- 422: validação falhou

### 2.1.7. Solicitar nova senha

**POST /auth/forgot-password**

Autenticação: não

Payload:

- username - required
  - pode ser email ou CPF

Resposta:

- 200: usuário sendo resetado (apenas id, name, email e CPF)
- 417: tenant não encontrado
- 422: validação falhou ou a requisição foi "estrangulada"

### 2.1.8. Alterar senha (reset token)

**POST /auth/reset-password**

Autenticação: não

Payload:

- username - required
  - pode ser email ou CPF
- token - required
- password - required|confirmed|Password::min(8)->uncompromised()

Resposta:

- 200: usuário + token
- 417: tenant não encontrado
- 422: validação falhou ou a requisição foi "estrangulada"

## 2.2. Conteúdo Global

- são conteúdos somente leitura, carregados sempre através de rotinas de importação;
- são conteúdos disponíveis a todos os *tenants* do sistema;
- substituir o parâmetro **TYPE** pelo tipo de recurso (detalhamento em **global.md**); e
- substituir o parâmetro **UUID** pelo identificador único do recurso solicitado.

### 2.2.1. Listar recursos

**GET /global/TYPE**

Autenticação: não

Query:

- page: número da página (se paginado), iniciando em 1
- size: tamanho das páginas (se configurado para ser paginado), iniciando em 0 (retornando tudo)

Resposta:

- 200: lista de itens
- 400: parâmetros inválidos na consulta
- 412: tipo não encontrado
- 417: tenant não encontrado

### 2.2.2. Visualizar recurso

**GET /global/TYPE/UUID**

Autenticação: não

Resposta:

- 200: item
- 400: parâmetros inválidos na consulta
- 404: item não encontrado
- 412: tipo não encontrado
- 417: tenant não encontrado



## 2.3. Conteúdo Local

- são conteúdos criados dentro do contexto de um *tenant* específico;
- o detalhamento dos tipos está em XX;
- substituir o parâmetro **TYPE** pelo tipo de recurso (detalhamento em **local.md**); e
- substituir o parâmetro **UUID** pelo identificador único do recurso solicitado.

### 2.3.1. Listar recursos

**GET /local/TYPE**

Autenticação: não

Regras de acesso:

- Usuários não autenticados
  - Recursos marcados com *private = 0* e *visible = 1*

- Usuários autenticados
  - Se for proprietário do recurso, pode visualizar
  - Se não for proprietário só pode visualizar os recursos marcados com *private = 0* e *visible = 1*

- Administradores autenticados
  - Se for proprietário do recurso, pode visualizar
  - Se não for proprietário só pode visualizar os recursos marcados com *private = 0*

Query:

- page: número da página (se paginado), iniciando em 1
- size: tamanho das páginas (se configurado para ser paginado), iniciando em 0 (retornando tudo)

Resposta:

- 200: lista de itens
- 400: parâmetros inválidos na consulta
- 412: tipo não encontrado
- 417: tenant não encontrado

### 2.3.2. Visualizar recurso

**GET /local/TYPE/UUID**

Autenticação: não

Regras de acesso:

- Usuários não autenticados
  - Recursos marcados com *private = 0* e *visible = 1*

- Usuários autenticados
  - Se for proprietário do recurso, pode visualizar
  - Se não for proprietário só pode visualizar os recursos marcados com *private = 0* e *visible = 1*

- Administradores autenticados
  - Se for proprietário do recurso, pode visualizar
  - Se não for proprietário só pode visualizar os recursos marcados com *private = 0*

Resposta:

- 200: item
- 400: parâmetros inválidos na consulta
- 404: item não encontrado
- 412: tipo não encontrado
- 417: tenant não encontrado

### 2.3.3. Criar recurso

**POST /local/TYPE**

Autenticação: sim

Resposta:

- 201: item
- 401: não autenticado
- 412: tipo não encontrado
- 417: tenant não encontrado

### 2.3.4. Atualizar recurso

**POST /local/TYPE/UUID**

Autenticação: sim

Regras de validação:

- Todas as regras fixas e as definidas pelo tipo do recurso são marcadas como *"sometimes"*, passando a serem verificadas apenas caso o campo seja enviado na requisição. Dessa maneira, é possível fazer atualizações pontuais no recurso, sem afetar os demais.

Regras de acesso:

- Se for proprietário do recurso, pode atualizar
- Se for administrador e não proprietário do recurso, pode atualizar se *private = 0*

Resposta:

- 200: item
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 412: tipo não encontrado
- 417: tenant não encontrado
- 422: validação falhou

### 2.3.5. Apagar recurso

**POST /local/TYPE/UUID**

Autenticação: sim

Regras de acesso:

- Se for proprietário do recurso, pode apagar
- Se for administrador e não proprietário do recurso, pode apagar se *private = 0*

Regras de negócio adicionais:

- Se o contéudo estiver sendo referenciado em uma equipe (*teams*), a equipe será apagada junto

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 412: tipo não encontrado
- 417: tenant não encontrado
- 422: validação falhou



## 2.4. Uploads

### 2.4.1. Carregar arquivo

**POST /upload**

Autenticação: sim

Payload:

- file
- name (opcional)

Resposta:

- 201: arquivo carregado com sucesso
- 401: não autenticado
- 417: tenant não encontrado
- 422: validação falhou

### 2.4.2. Detalhes do arquivo

**GET /upload/UUID**

Autenticação: não

Resposta:

- 200: detalhes do arquivo
- 404: arquivo não encontrado
- 417: tenant não encontrado

### 2.4.3. Apagar arquivo

**POST /upload/UUID**

Autenticação: sim

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 417: tenant não encontrado



## 2.5. Variáveis de Usuário

### 2.5.1. Listar variáveis de um grupo

**GET /variables/GROUP**

Autenticação: sim

Resposta:

- 200: lista de variáveis do grupo *GROUP*
- 401: não autenticado
- 417: tenant não encontrado

### 2.5.2. Criar ou atualizar variáveis em batch em um GRUPO

**POST /variables/GROUP**

Autenticação: sim

Payload:

- KEY1: value
- KEY2: value
- ...
- KEYn: value

Resposta:

- 200: lista com as variáveis criadas ou atualizadas no grupo *GROUP*
- 401: não autenticado
- 417: tenant não encontrado

### 2.5.3. Apagar variáveis em batch de um grupo

**POST /variables/GROUP**

Autenticação: sim

Payload:

- _method: DELETE
- keys: lista de KEYs para apagar (se vazia, todo GRUPO é apagado)

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 417: tenant não encontrado

### 2.5.4. Criar ou atualizar uma variável no grupo

**POST /variables/GROUP/KEY**

Autenticação: sim

Payload:

- value

Resposta:

- 200: informações da variável atualizada na chave *KEY* do grupo *GROUP*
- 201: informações da variável criada
- 401: não autenticado
- 417: tenant não encontrado

### 2.5.5. Recuperar uma variável de um grupo

**GET /variables/GROUP/KEY**

Autenticação: sim

Resposta:

- 200: informações da variável da chave *KEY* do grupo *GROUP*
- 401: não autenticado
- 404: não encontrado
- 417: tenant não encontrado

### 2.5.6. Apagar variável de um grupo

**POST /variables/GROUP/KEY**

Autenticação: sim

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 404: não encontrado
- 417: tenant não encontrado



## 2.6. Mail

### 2.6.1. Enviar email

**POST /mail**

Autenticação: sim

Payload:

- to - array de emails
- cc - array de emails
- bcc - array de emails
- subject
- body (conteúdo HTML)
- text (conteúdo text)

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado

### 2.6.2. Listar administradores

**GET /mail/admin-users**

Autenticação: sim

Resposta:

- 200: lista de administradores
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado



## 2.7. Administração de Usuários

### 2.7.1. Listar usuários

**GET /admin/users**

Autenticação: sim, administrador

Query:

- page: número da página, iniciando em 1
- size: tamanho das páginas (padrão, 20), iniciando em 0 (retornando tudo)

Includes:

- teams
- teams.workshop
- teams.projects
- certificates

Filtros:

- id: exact
- name: partial
- email: partial
- phone: partial
- cpf: exact
- active: exact (1 = usuários ativos / 0 = usuários apagados)
- level: exact ('deleted', 'admin 'ou 'user')
- search: scope
- allow_email_messages: exact (true ou false - usuários sem a propriedade são ignorados)

Ordenação:

- name (padrão)
- created_at
- updated_at

Resposta:

- 200: lista de usuários
- 400: parâmetros inválidos na consulta
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado

### 2.7.2. Visualizar usuário

**GET /admin/users/UUID**

Autenticação: sim, administrador

Includes:

- teams
- teams.workshop
- teams.projects
- certificates

Resposta:

- 200: usuário
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 417: tenant não encontrado

### 2.7.3. Criar usuário

**POST /admin/users**

Autenticação: sim, administrador

Payload:

- name - required|string|max:191
- email - required|string|max:191|email|unique:users
- phone - nullable|string|max:191
- cpf - required|string|max:191|cpf|unique:users
- teams - sometimes|array
- teams.* - nullable|exists:team,id
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 201: usuário
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou

### 2.7.4. Atualizar usuário

**POST /admin/users/UUID**

Autenticação: sim, administrador

Payload:

- name - sometimes|required|string|max:191
- email - sometimes|required|string|max:191|email|unique:users
- phone - sometimes|nullable|string|max:191
- cpf - sometimes|required|string|max:191|cpf|unique:users
- teams - sometimes|array
- teams.* - sometimes|nullable|exists:team,id
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 200: usuário
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 417: tenant não encontrado
- 422: validação falhou

### 2.7.5. Apagar usuário

**POST /admin/users/UUID**

Autenticação: sim, administrador

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 417: tenant não encontrado

### 2.7.6. Validar usuários em lote

**POST /admin/users/batch/validate**

Autenticação: sim, administrador

Payload:

- file - required|file|mimes:xlsx

Resposta:

- 200: lista de possíveis candidatos a importação
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 422: validação falhou
- 417: tenant não encontrado

### 2.7.7. Importar usuários em lote

**POST /admin/users/batch/import**

Autenticação: sim, administrador

Payload:

- users - required|array
- users.*.name - required|string|max:191
- users.*.cpf - required|string|cpf|unique:users,cpf
- users.*.phone - nullable|string|max:191
- users.*.email - required|string|max:11|email|unique:users,email

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 422: validação falhou
- 417: tenant não encontrado



## 2.8. Administração de Equipes

### 2.8.1. Listar equipes

**GET /admin/teams**

Autenticação: sim, administrador

Query:

- page: número da página, iniciando em 1
- size: tamanho das páginas (padrão, 20), iniciando em 0 (retornando tudo)

Includes:

- users
- workshop
- projects

Filtros:

- id: exact
- name: partial
- ref: exact
- search: scope
- states: scope (lista de estados - precisa estar com o mesmo case cadastrado na UBS)

Ordenação:

- name (padrão)
- created_at
- updated_at

Resposta:

- 200: lista de equipes
- 400: parâmetros inválidos na consulta
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado

### 2.8.2. Visualizar equipe

**GET /admin/teams/UUID**

Autenticação: sim, administrador

Includes:

- users
- workshop
- projects

Resposta:

- 200: equipe
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 417: tenant não encontrado

### 2.8.3. Criar equipe

**POST /admin/teams**

Autenticação: sim, administrador

Payload:

- name - required|string|max:191
- ref_id - nullabe|exists:content,id
- users - sometimes|array
- users.* - nullable|exists:users,id
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 201: equipe
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou

### 2.8.4. Atualizar equipe

**POST /admin/teams/UUID**

Autenticação: sim, administrador

Payload:

- name - sometimes|required|string|max:191
- ref_id - sometimes|nullabe|exists:content,id
- users - sometimes|array
- users.* - sometimes|nullable|exists:users,id
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 200: equipe
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 417: tenant não encontrado
- 422: validação falhou

### 2.8.5. Apagar equipe

**POST /admin/teams/UUID**

Autenticação: sim, administrador

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 417: tenant não encontrado



## 2.9. Equipes do usuário

Apenas equipes nas quais o usuário está associado são retornadas.

### 2.9.1. Listar equipes

**GET /teams**

Autenticação: sim

Query:

- page: número da página (se paginado), iniciando em 1

Includes:

- users
- workshop
- projects

Filtros:

- id - exact
- name - partial
- ref - exact

Sorts:

- name (default)
- created_at
- updated_at

Resposta:

- 200: lista de equipes
- 400: parâmetros inválidos na consulta
- 417: tenant não encontrado

### 2.9.2. Visualizar equipe

**GET /teams/UUID**

Autenticação: sim

Includes:

- users
- workshop
- projects

Resposta:

- 200: equipe
- 400: parâmetros inválidos na consulta
- 404: item não encontrado
- 417: tenant não encontrado



## 2.10. Anotações

### 2.10.1. Listar anotações

**GET /notes**

Autenticação: sim

Query:

- page: número da página, iniciando em 1
- size: tamanho das páginas (padrão, 20), iniciando em 0 (retornando tudo)

Filtros:

- id: exact
- title: partial
- body: partial
- search: scope (busca em title & body)

Ordenação:

- title
- created_at  (padrão, decrescente)
- updated_at

Resposta:

- 200: lista de anotações
- 400: parâmetros inválidos na consulta
- 422: validação falhou
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado

### 2.10.2. Visualizar anotação

**GET /notes/UUID**

Autenticação: sim

Resposta:

- 200: anotação
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 417: tenant não encontrado

### 2.10.3. Criar anotação

**POST /notes**

Autenticação: sim

Payload:

- title - sometimes|nullable|string
- body - sometimes|nullabe|string
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 201: anotação
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou

### 2.10.4. Atualizar anotação

**POST /notes/UUID**

Autenticação: sim

Payload:

- title - sometimes|nullable|string
- body - sometimes|nullable|string
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 200: anotação
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 417: tenant não encontrado
- 422: validação falhou

### 2.10.5. Apagar anotação

**POST /notes/UUID**

Autenticação: sim

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 417: tenant não encontrado



## 2.11. Projetos

### 2.11.1. Criar projeto

**POST /projects**

Autenticação: sim, administrador

Payload:

- team_id - required|exists:teams,id

Resposta:

- 201: projeto
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou

### 2.11.2. Listar projetos

**GET /projects**

Autenticação: sim

Regras de negócio:

- administradores conseguem visualizar qualquer projeto
- usuários conseguem visualizar qualquer projeto público
- usuários conseguem visualizar os projetos pertecentes as suas equipes

Query:

- page: número da página, iniciando em 1
- size: tamanho das páginas (padrão, 20), iniciando em 0 (retornando tudo)

Includes:

- team
- team.users
- team.workshop
- posts

Filtros:

- id: exact
- title: partial
- description: partial
- visible: exact
- states: scope (lista de estados - precisa estar com o mesmo case cadastrado na UBS)

Ordenação:

- title
- created_at  (padrão, decrescente)
- updated_at

Resposta:

- 200: lista de projetos
- 400: parâmetros inválidos na consulta
- 422: validação falhou
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado

### 2.11.3. Visualizar projeto

**GET /projects/UUID**

Autenticação: sim

Regras de negócio:

- administradores conseguem visualizar qualquer projeto
- usuários conseguem visualizar qualquer projeto público
- usuários conseguem visualizar os projetos pertecentes as suas equipes

Includes:

- team
- team.users
- team.workshop
- posts

Resposta:

- 200: projeto
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 417: tenant não encontrado

### 2.11.4. Atualizar projeto

**POST /projects/UUID**

Autenticação: sim

Regras de negócio:

- administradores conseguem atualizar qualquer projeto e em qualquer situação
- usuários conseguem atualizar projetos pertencentes as suas equipes e desde que estejam invisíveis
- apenas administradores podem alterar a visibilidade de um projeto

Payload:

- title - sometimes|nullable|string|max:191
- description - sometimes|nullable|string
- cover_url - sometimes|nullable|url
- visible - sometimes|boolean
- outros campos enviados não são validados no momento, mas são salvos no campo *properties*.

Resposta:

- 200: projeto
- 401: não autenticado
- 403: não autorizado
- 404: não encontrado
- 417: tenant não encontrado
- 422: validação falhou

### 2.11.5. Apagar projeto

**POST /projects/UUID**

Autenticação: sim, administrador

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 417: tenant não encontrado

### 2.11.6. Criar postagem

**POST /projects/UUID/posts**

Autenticação: sim

Detalhamento:

- o upload deve ser feito através de um formulário multipart (multipar/form-data)
- os arrays são identificados através de "[]" no final do nome do campo - ex: photos[], files[]

Regras de negócio:

- administradores conseguem criar postagens em qualquer projeto
- membros da equipe conseguem criar postagens em seus projetos

Payload:

- title - required|string
- body - nullabe|string
- type - required|string|max:191
- images - nullable|array
- images.* - required|image

Resposta:

- 201: post
- 401: não autenticado
- 403: não autorizado
- 404: projeto não encontrado
- 409: projeto já publicado
- 417: tenant não encontrado
- 422: validação falhou

### 2.11.7. Atualizar postagem

**POST /projects/UUID/posts/UUID**

Autenticação: sim

Detalhamento:

- o upload deve ser feito através de um formulário multipart (multipar/form-data)
- os arrays são identificados através de "[]" no final do nome do campo - ex: photos[], files[]
- durante a atualização, inicialmente será configurado o campo "photos" no objeto com os valores enviados na requisição e posteriormente será realizado o merge dos arquivos recebidos por upload (append)

Regras de negócio:

- administradores conseguem atualizar qualquer postagem do sistema
- apenas o dono da postagem consegue atualizar a postagem
- o dono precisa ser membro da equipe do projeto

Payload:

- title - sometimes|required|string
- body - sometimes|nullabe|string
- type - sometimes|required|string|max:191
- photos - sometimes|nullable|array
- images - sometimes|nullable|array
- images.* - required|image

Resposta:

- 200: post
- 401: não autenticado
- 403: não autorizado
- 404: projeto ou post não encontrado
- 409: projeto já publicado
- 417: tenant não encontrado
- 422: validação falhou

### 2.11.8. Apagar postagem

**POST /projects/UUID/posts/UUID**

Autenticação: sim

Regras de negócio:

- administradores conseguem apagar qualquer postagem do sistema
- apenas o dono da postagem consegue apagar a postagem
- o dono precisa ser membro da equipe do projeto

Payload:

- _method: DELETE

Resposta:

- 204: sem conteúdo
- 401: não autenticado
- 403: não autorizado
- 404: projeto ou post não encontrado
- 409: projeto já publicado
- 417: tenant não encontrado

### 2.11.9. Listar postagens

**GET /projects/UUID/posts**

Autenticação: sim

Regras de negócio:

- administradores conseguem visualizar postagens de qualquer projeto
- usuários conseguem visualizar visualizar postagens de qualquer projeto público
- usuários conseguem visualizar as postagens de qualquer projeto da sua equipe

Query:

- page: número da página, iniciando em 1
- size: tamanho das páginas (padrão, 20), iniciando em 0 (retornando tudo)

Filtros:

- id: exact
- title: partial
- body: partial
- type: exact
- search: scope

Ordenação:

- title
- created_at  (padrão, decrescente)
- updated_at

Resposta:

- 200: lista de postagens
- 400: parâmetros inválidos na consulta
- 422: validação falhou
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado

### 2.11.10. Visualizar postagem

**GET /projects/UUID/posts/UUID**

Autenticação: sim

Regras de negócio:

- administradores conseguem visualizar postagens de qualquer projeto
- usuários conseguem visualizar visualizar postagens de qualquer projeto público
- usuários conseguem visualizar as postagens de qualquer projeto da sua equipe

Resposta:

- 200: postagem
- 401: não autenticado
- 403: não autorizado
- 404: item não encontrado
- 417: tenant não encontrado

### 2.11.11. Gerar certificado

**POST /projects/UUID/certificate**

Autenticação: sim, administrador

Payload:

- template - required|string
- parameters - nullable|array

Resposta:

- 200: certificado criado / atualizado (temporário)
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou



## 2.12. Avaliação de qualidade

### 2.12.1. Criar avaliação

**POST /quality-survey**

Autenticação: sim

Payload:

- ref_id - required|exists:contents,id
- question1 - required|string
- question2 - required|string
- ...
- question11 - required|string
- question12 - required|string

Resposta:

- 204: avaliação criada (ou atualizada)
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou



## 2.13. Certificados

### 2.13.1. Gerar certificado

**POST /certificates**

Autenticação: sim

Payload:

- ref_id - required|exists:contents,id
- template - required|string
- parameters - required|array

Resposta:

- 200: certificado atualizado (temporário)
- 201: certificado criado
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou

### 2.13.2. Visualizar certificado

**GET /certificates/UUID**

Autenticação: não

Resposta:

- 200: certificado
- 404: não encontrado
- 417: tenant não encontrado



## 2.14. Analytics

### 2.14.1. Registrar navegação

**POST /analytics**

Autenticação: sim

Payload:

- type - required|string|max:255
- label1 - required|string|max:255
- label2 - nullable|string|max:255
- label3 - nullable|string|max:255
- city - nullable|string|max:255
- ubs - nullable|string|max:255
- progress - nullable|integer|min:0|max:100

Resposta:

- 204: registro criado
- 401: não autenticado
- 403: não autorizado
- 417: tenant não encontrado
- 422: validação falhou
- 500: erro desconhecido
