<?php

return [
    'tenants' => [
        '94632844-067a-466c-ad06-3c14db9e1c58' => [
            'id' => 1,
            'name' => 'CEDAPS Virtual',
            'frontend_url' => 'https://blissful-fermi-d673c5.netlify.app',
        ],
        '11255f7c-1dc3-4b86-9900-7681c2167090' => [
            'id' => 2,
            'name' => 'Ciclo Virtual',
            'frontend_url' => 'https://eloquent-shirley-2089b9.netlify.app',
        ],
    ],
];
