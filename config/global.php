<?php

return [
    'types' => [
        'states',
        'cities',
    ],

    'states' => [
        'relations' => [
            'cities' => [
                'type' => 'hasMany',
                'filters' => [
                    'type' => 'cities'
                ],
            ],
        ],
        'includes' => [
            'cities' => 'many',
        ],
        'orders' => [],
        'order' => 'name',
        'page-size' => 0,
    ],

    'cities' => [
        'relations' => [
            'state' => [
                'type' => 'belongsTo',
                'filters' => [
                    'type' => 'states',
                ],
            ],
        ],
        'includes' => [
            'state' => 'single',
        ],
        'filters' => [
            \Spatie\QueryBuilder\AllowedFilter::exact('state_id', 'properties->state_id'),
        ],
        'orders' => [
            \Spatie\QueryBuilder\AllowedSort::field('state_id', 'properties->state_id'),
        ],
        'order' => 'name',
        'page-size' => 25,
        'can-be-orphan' => false,
    ],
];
