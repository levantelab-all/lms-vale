<?php

return [
    'mode'                  => '',
    'format'                => 'A4',
    'defaultFontSize'       => '',
    'defaultFont'           => 'Sans Serif',
    'marginLeft'            => 20,
    'marginRight'           => 20,
    'marginTop'             => 20,
    'marginBottom'          => 20,
    'marginHeader'          => 0,
    'marginFooter'          => 0,
    'orientation'           => 'L',

    'title'                 => 'Plataforma de Formação - MEC',
    'author'                => 'Ministério da Educação',
    'watermark'             => '',
    'showWatermark'         => false,
    'watermarkFont'         => 'DejaVuSansCondensed',
    'displayMode'           => 'fullpage',
    'watermarkTextAlpha'    => 0.1,

    'protection'            => [
        /*
        | SetProtection – Encrypts and sets the PDF document permissions
        |
        | https://mpdf.github.io/reference/mpdf-functions/setprotection.html
        */
        'permissions' => [
            'copy' => true,
            'print' => true,
            'modify' => false,
            'annot-forms' => false,
            'fill-forms' => false,
            'extract' => false,
            'assemble' => false,
            'print-highres' => false,
        ],
        'user_password' => null,
        'owner_password' => null,
        'length' => 40,
    ],
];
