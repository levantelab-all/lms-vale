<?php

return [
    'types' => [
        'ubs',

        'workshops',
        'modules',
        'activities',
        'evaluations',
        'questions',

        'roles',

        'documents',
        'open-projects',
    ],

    'ubs' => [
        'fields' => [
            'state_id' => 'required|uuid',
            'state' => 'required|string',
            'city_id' => 'required|uuid',
            'city' => 'required|string',
        ],
        'relations' => [
        ],
        'includes' => [
            'teams' => 'many',
            'teams.users' => 'many',
        ],
        'filters' => [
            \Spatie\QueryBuilder\AllowedFilter::exact('state_id', 'properties->state_id'),
            \Spatie\QueryBuilder\AllowedFilter::partial('state', 'properties->state'),
            \Spatie\QueryBuilder\AllowedFilter::exact('city_id', 'properties->city_id'),
            \Spatie\QueryBuilder\AllowedFilter::partial('city', 'properties->city'),
            \Spatie\QueryBuilder\AllowedFilter::exact('teams.id', 'teams.uuid'),
            \Spatie\QueryBuilder\AllowedFilter::scope('teams'),
            \Spatie\QueryBuilder\AllowedFilter::callback('search', function ($query, $term, $property) {
                $term = Illuminate\Support\Str::lower('%' . str_replace(' ', '%', $term) . '%');

                return $query->where(function ($q) use ($term) {
                    return $q->where('contents.name', 'LIKE', $term)
                        ->orWhere(\DB::raw('LOWER(JSON_UNQUOTE(JSON_EXTRACT(contents.properties, \'$."city"\')))'), 'LIKE', $term)
                        ->orWhere(\DB::raw('LOWER(JSON_UNQUOTE(JSON_EXTRACT(contents.properties, \'$."state"\')))'), 'LIKE', $term)
                        ->orWhereHas('teams', function ($q2) use ($term) {
                            return $q2->where('teams.name', 'LIKE', $term);
                        });
                });
            }),
        ],
        'orders' => [
            \Spatie\QueryBuilder\AllowedSort::field('state', 'properties->state'),
            \Spatie\QueryBuilder\AllowedSort::field('city', 'properties->city'),
        ],
        'order' => 'name',
        'page-size' => 25,
        'can-be-orphan' => true,
    ],

    'workshops' => [
        'fields' => [
            'type' => 'required|string|in:basic,thematic',
            'description' => 'nullable|string',
            'cover_url' => 'nullable|url',
            'dedication_time' => 'required|integer|min:0',
        ],
        'relations' => [
            'modules' => [
                'type' => 'hasMany',
                'filters' => [
                    'type' => 'modules',
                ],
                'orders' => [
                    'order' => 'asc',
                ],
            ],
            'evaluations' => [
                'type' => 'hasMany',
                'filters' => [
                    'type' => 'evaluations',
                ],
            ],
        ],
        'includes' => [
            'modules' => 'many',
            'evaluations' => 'many',
        ],
        'filters' => [
            \Spatie\QueryBuilder\AllowedFilter::exact('type', 'properties->type'),
        ],
        'orders' => [],
        'order' => 'name',
        'page-size' => 12,
    ],

    'modules' => [
        'fields' => [
            'type' => 'required|string|in:content,diary'
        ],
        'relations' => [
            'activities' => [
                'type' => 'hasMany',
                'filters' => [
                    'type' => 'activities',
                ],
                'orders' => [
                    'order' => 'asc',
                ],
            ],
            'workshop' => [
                'type' => 'belongsTo',
                'filters' => [
                    'type' => 'workshops',
                ],
            ],
        ],
        'includes' => [
            'activities' => 'many',
            'workshop' => 'single',
        ],
        'filters' => [
            \Spatie\QueryBuilder\AllowedFilter::exact('type', 'properties->type'),
        ],
        'orders' => [],
        'order' => 'name',
        'page-size' => 0,
        'can-be-orphan' => false,
    ],

    'activities' => [
        'fields' => [
            'type' => 'required|string|in:image,text,embed,video,document',
            'body' => 'nullable',
            'url' => 'required_if:type,image,video,document|nullable|url',
            'embed' => 'required_if:type,embed|nullable',
        ],
        'relations' => [
            'module' => [
                'type' => 'belongsTo',
                'filters' => [
                    'type' => 'modules',
                ],
            ],
        ],
        'includes' => [
            'module' => 'single',
        ],
        'filters' => [
            \Spatie\QueryBuilder\AllowedFilter::exact('type', 'properties->type'),
        ],
        'orders' => [],
        'order' => 'name',
        'page-size' => 0,
        'can-be-orphan' => false,
    ],

    'evaluations' => [
        'fields' => [
            'description' => 'nullable',
            // 'score' => 'required|integer|min:0|max:10',
        ],
        'relations' => [
            'questions' => [
                'type' => 'hasMany',
                'filters' => [
                    'type' => 'questions',
                ],
                'orders' => [
                    'order' => 'asc',
                ],
            ],
            'workshop' => [
                'type' => 'belongsTo',
                'filters' => [
                    'type' => 'workshops',
                ],
            ],
        ],
        'includes' => [
            'questions' => 'many',
            'workshop' => 'single',
        ],
        'filters' => [
        ],
        'orders' => [],
        'order' => 'name',
        'page-size' => 0,
        'can-be-orphan' => false,
    ],

    'questions' => [
        'fields' => [
            'type' => 'required|string|in:boolean,multi-choice',
            'question' => 'required|string',
            'feedback' => 'nullable|string',
            'options.*' => 'required_if:type,multi-choice|nullable|array',
            'options.*.id' => 'required|integer',
            'options.*.text' => 'required|string',
            // 'correct-option' => 'required_if:type,multi-choice|nullable|integer',
            // 'correct' => 'required_if:type,boolean|boolean',
        ],
        'relations' => [
            'evaluation' => [
                'type' => 'belongsTo',
                'filters' => [
                    'type' => 'evaluations',
                ],
            ],
        ],
        'includes' => [
            'evaluation' => 'single',
        ],
        'filters' => [
        ],
        'orders' => [],
        'order' => 'name',
        'page-size' => 0,
        'can-be-orphan' => false,
    ],

    'roles' => [
        'fields' => [
        ],
        'relations' => [
        ],
        'includes' => [
        ],
        'filters' => [
        ],
        'orders' => [
        ],
        'order' => 'name',
        'page-size' => 25,
        'can-be-orphan' => true,
    ],

    'documents' => [
        'fields' => [
            'type' => 'required|string|in:document,image,link,video,embed',
            'url' => 'required_if:type,document,image,link,video|string|url',
            'embed' => 'required_if:type,embed|string',
            'thumb' => 'nullable|string|url',
            'summary' => 'nullable|string',
        ],
        'relations' => [
        ],
        'includes' => [
        ],
        'filters' => [
            \Spatie\QueryBuilder\AllowedFilter::exact('type'),
            \Spatie\QueryBuilder\AllowedFilter::callback('search', function ($query, $term, $property) {
                $term = Illuminate\Support\Str::lower('%' . str_replace(' ', '%', $term) . '%');

                return $query->where(function ($q) use ($term) {
                    return $q->where('contents.name', 'LIKE', $term)
                        ->orWhere(\DB::raw('LOWER(JSON_UNQUOTE(JSON_EXTRACT(contents.properties, \'$."summary"\')))'), 'LIKE', $term);
                });
            }),
        ],
        'orders' => [
        ],
        'order' => 'name',
        'page-size' => 25,
        'can-be-orphan' => true,
    ],
    'open-projects' => [
        'fields' => [
            //'title' => 'required|string|',
            'created_by_id' => 'required|string',
            'date' => 'nullable|string',
            'team' => 'nullable|string',
            'city' => 'nullable|string',
            'care_line' => 'nullable|string',
            'what' => 'nullable|string',
            'where' => 'nullable|string',
            'results' => 'nullable|string',
            'visible' => 'nullable|string',
            'thumb' => 'nullable|string|url',
        ],
        'relations' => [
        ],
        'includes' => [
        ],
        'filters' => [
            \Spatie\QueryBuilder\AllowedFilter::exact('type', 'properties->type'),
            \Spatie\QueryBuilder\AllowedFilter::exact('care_line', 'properties->care_line'),
            \Spatie\QueryBuilder\AllowedFilter::exact('city', 'properties->city'),
        ],
        'orders' => [
        ],
        'order' => 'name',
        'page-size' => 25,
        'can-be-orphan' => true,
    ],
];
